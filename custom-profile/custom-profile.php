<?php
/*
Plugin Name: custom profile form
Description: custom profile form
Version: 1.0.0
Plugin URI: https://www.fiverr.com/wp_right  
Author: LogicsBuffer
Author URI: http://logicsbuffer.com/
*/
// To change add to cart text on single product page
/* add_filter( 'woocommerce_product_single_add_to_cart_text', 'woocommerce_custom_single_add_to_cart_text' ); 
function woocommerce_custom_single_add_to_cart_text() {
    return __( 'Finish', 'woocommerce' ); 
} */

// function give_profile_name($atts){
    // $user = wp_get_current_user();
    // $name = $user->user_firstname; 
    // return $name;
// }

// add_shortcode('profile_name', 'give_profile_name');
function wpdocs_theme_slug_widgets_init() {
    register_sidebar( array(
        'name'          => __( 'Measurement Sidebar', 'textdomain' ),
        'id'            => 'measurement-1',
        'description'   => __( 'Widgets in this area will be shown measurement page.', 'textdomain' ),
        'before_widget' => '<li id="%1$s" class="widget %2$s">',
        'after_widget'  => '</li>',
        'before_title'  => '<h2 class="widgettitle">',
        'after_title'   => '</h2>',
    ) );
}
add_action( 'widgets_init', 'wpdocs_theme_slug_widgets_init' );
add_filter( 'wp_nav_menu_objects', 'my_dynamic_menu_items' );
function my_dynamic_menu_items( $menu_items ) {
    foreach ( $menu_items as $menu_item ) {
        if ( '#profile_name#' == $menu_item->title ) {  
		$user = wp_get_current_user();
		$name = $user->display_name; 
                $menu_item->title = $name;
        }
    }

    return $menu_items;
} 
add_action( 'woocommerce_before_add_to_cart_button', 'misha_before_add_to_cart_btn' );
 
function misha_before_add_to_cart_btn(){
	$post_ID = get_the_ID();
	if (has_term('customise','product_cat')){
	?>
<input id="customize_btn" class="cssButton submit_button button  button_in_cart" type="button" value="Customize Now">
<div id="wizard_div" style="display:none;">
	<div style="text-align: center;">
		<button type="button" id="prevBtn" class="btn btn-header" onclick="nextPrev(-1)">Previous</button>
	  <!--   <button type="button" id="skipBtn" class="btn info" onclick="nextPrev(1)">Skip</button>-->
		<button type="button" id="nextBtn" class="btn btn-header" onclick="nextPrev(1)">Next</button>
	</div>
	  
	<!-- Circles which indicates the steps of the form: -->
	<div style="text-align:center;margin-top:10px;">
	  <span class="step"></span>
	  <span class="step"></span>
	  <span class="step"></span>
	  <span class="step"></span>
	  <span class="step"></span>
	  <span class="step"></span>
	  <span class="step"></span>
	  <span class="step"></span>
	</div>
</div>
<script>
jQuery(document).ready(function () {
jQuery("#customize_btn").click(function(){
	jQuery("#wizard_div,table.variations").show();
	jQuery("#customize_btn,.summary-before").hide();
	jQuery(".summary.entry-summary").css({"width": "100%"});

});
jQuery("button.single_add_to_cart_button").text("Finish");
//jQuery("button.single_add_to_cart_button").hide();

jQuery(".variations tbody").append('<tr><td class="label"><label for="pa_back-pocket-style"></label></td><td class="value"></td></tr><tr><td class="label"><label for="pa_back-pocket-style">Back Pocket Style</label></td><td class="value"></td></tr><tr><td class="label"></td><td class="value"></td></tr>');
});
console.log('asda');
var currentTab = 0; // Current tab is set to be the first tab (0)
showTab(currentTab); // Display the current tab

function showTab(n) {
	console.log(n);
  // This function will display the specified tab of the form ...
  var x = jQuery(".variations tbody tr");
  x[n].style.display = "block";
  jQuery(".quantity,button.single_add_to_cart_button").css("display","none");
  //jQuery(".sp-container.sp-light.sp-input-disabled.sp-palette-buttons-disabled.sp-palette-only.sp-initial-disabled.sp-hidden").css("display","none !important");
  //,.wccpf-fields-group-2
  // ... and fix the Previous/Next buttons:
  if (n == 5) {
	jQuery(".wccpf-fields-group-1").show();
	jQuery(".wccpf-fields-group-2").hide();
	jQuery(".wccpf-fields-group-3").hide();
  }else if(n == 6){
	jQuery(".wccpf-fields-group-1").hide();
	jQuery(".wccpf-fields-group-2").show();
	jQuery(".wccpf-fields-group-3").hide();
	//jQuery(".sp-container.sp-light.sp-input-disabled.sp-palette-buttons-disabled.sp-palette-only.sp-initial-disabled.sp-hidden").css("display","block !important");

  }else if(n == 7){
	jQuery(".wccpf-fields-group-1").hide();
	jQuery(".wccpf-fields-group-2").hide();
	jQuery(".wccpf-fields-group-3").show();
	jQuery(".quantity,button.single_add_to_cart_button").css("display","block");
	
  }else{
	jQuery(".wccpf-fields-group-1").hide();
	jQuery(".wccpf-fields-group-2").hide();
  }
  if (n == 0) {
    document.getElementById("prevBtn").style.display = "none";
  } else {
    document.getElementById("prevBtn").style.display = "inline";
    document.getElementById("nextBtn").style.display = "inline";
  //  document.getElementById("skipBtn").style.display = "inline";
  }
  if (n == (x.length - 1)) {
    document.getElementById("nextBtn").style.display = "none"
    //document.getElementById("skipBtn").style.display = "none"
  } else {
    document.getElementById("nextBtn").innerHTML = "Next";
  }
  // ... and run a function that displays the correct step indicator:
  fixStepIndicator(n)
}

function nextPrev(n) {
  // This function will figure out which tab to display
  var x = jQuery(".variations tbody tr");
  // Exit the function if any field in the current tab is invalid:
  if (n == 1 && !validateForm()) return false;
  // Hide the current tab:
  x[currentTab].style.display = "none";
  // Increase or decrease the current tab by 1:
  currentTab = currentTab + n;
  // if you have reached the end of the form... :
 // var form_end = x.length +2;
  if (currentTab >= x.length) {
    //...the form gets submitted:
    document.getElementById("regForm").submit();
    return false;
  }
  // Otherwise, display the correct tab:
  showTab(currentTab);
}

function validateForm() {
  // This function deals with validation of the form fields
  var x, y, i, valid = true;
/*   x = jQuery(".variations tbody tr");
  y = x[currentTab].getElementsByTagName("input");
  // A loop that checks every input field in the current tab:
  for (i = 0; i < y.length; i++) {
    // If a field is empty...
    if (y[i].value == "") {
      // add an "invalid" class to the field:
      y[i].className += " invalid";
      // and set the current valid status to false:
      valid = false;
    }
  }
  // If the valid status is true, mark the step as finished and valid:
  if (valid) {
    document.getElementsByClassName("step")[currentTab].className += " finish";
  } */
  return valid; // return the valid status
}

function fixStepIndicator(n) {
  // This function removes the "active" class of all steps...
  var i, x = document.getElementsByClassName("step");
  for (i = 0; i < x.length; i++) {
    x[i].className = x[i].className.replace("active", "");
  }
  //... and adds the "active" class to the current step:
  x[n].className += " active";
}
//});
</script>
 <?php
 }else{
		//echo 'asfasdf';
		//echo 'asfasdf';
	}
}
add_action('wp_enqueue_scripts', 'geo_my_wp_script_front_css');
add_action('wp_enqueue_scripts', 'geo_my_wp_script_front_js');
add_action('admin_enqueue_scripts', 'geo_my_wp_script_back_css');
add_action('init', 'wp_astro_init');
function wp_astro_init() {
   add_shortcode('show_geomywp_form', 'wp_geo_my_wp_form');
}
function wp_geo_my_wp_form($atts) {
if(is_user_logged_in()){
	//$fit_style_arr = array('Measurement Style 1','Measurement Style 2','Measurement Style 3');
	$user_id = get_current_user_id();
	if(isset($_POST['submit_measurement'])){
		$user_id = get_current_user_id();
		$user_data = $_POST['id'];
		$user_name = $user_data['user_name'];
		$gender = $user_data['gender'];
		$fit_arr = $user_data['fit'];
		$fit_count = count($fit_arr);
		$length_arr = $user_data['length'];
		$waist_arr = $user_data['waist'];
		$seat_arr = $user_data['seat'];
		$front_rise_arr = $user_data['front_rise'];
		$back_rise_arr = $user_data['back_rise'];
		$thighs_arr = $user_data['thighs'];
		$knee_arr = $user_data['knee'];
		$leg_opening_arr = $user_data['leg_opening'];
	
		
		$error_msg= "<p class='alert alert-danger'>Sorry ! No measurement added Click to start.</p>";
		/* ?><pre><?php print_r($fit_arr); ?></pre><?php
		?><pre><?php print_r($fit_count); ?></pre><?php */
		if($fit_arr){
			if($fit_count > 3){

				$fit_arr = array_slice($fit_arr,-3);
				$length_arr = array_slice($length_arr,-3);
				$waist_arr = array_slice($waist_arr,-3);
				$seat_arr = array_slice($seat_arr,-3);
				$front_rise_arr = array_slice($front_rise_arr,-3);
				$back_rise_arr = array_slice($back_rise_arr,-3);
				$thighs_arr = array_slice($thighs_arr,-3);
				$knee_arr = array_slice($knee_arr,-3);
				$leg_opening_arr = array_slice($leg_opening_arr,-3);
				echo "<p class='alert alert-info'>Sorry ! you can not add more than 3 measurements.</p>";
			}
		
			$fit = implode("|",$fit_arr);
			$length = implode("|",$length_arr);
			$waist = implode("|",$waist_arr);
			$seat = implode("|",$seat_arr);
			$front_rise = implode("|",$front_rise_arr);
			$back_rise = implode("|",$back_rise_arr);
			$thighs = implode("|",$thighs_arr);
			$knee = implode("|",$knee_arr);
			$leg_opening = implode("|",$leg_opening_arr);
			$fit = implode("|",$fit_arr);
			$profile_name = $user_data['profile_name'];
			$success_msg = "<p class='alert alert-success'>Your measurements has been saved successfully</p>";
			$user_id = 'user_'.$user_id;
			update_field( 'gender', $gender, $user_id );
			update_field( 'fit', $fit, $user_id );
			update_field( 'length', $length, $user_id );
			update_field( 'waist', $waist, $user_id );
			update_field( 'seat', $seat, $user_id );
			update_field( 'front_rise', $front_rise, $user_id );
			update_field( 'thighs', $thighs, $user_id );
			update_field( 'knee', $knee, $user_id );
			update_field( 'leg_opening', $leg_opening, $user_id );
			update_field( 'back_rise', $back_rise, $user_id );
			update_field( 'profile_name', $profile_name, $user_id );
			update_field( 'success_msg',$success_msg, $user_id );
			
			//header("Refresh:0");
			echo $success_msg;
		}else{
			echo $error_msg;
		}
	}
	$user_id = get_current_user_id();
	$userdata =get_userdata($user_id);
	$user_name = $userdata->data->display_name;
	$user_id = 'user_'.$user_id;
	/* 	
		if($user_name){
			$user_name = $user_name;
		}else{
			$user_name = $_POST['user_name'];
		} */	
		$gender = get_field( 'gender',  $user_id );
		if($gender){
			$gender = $gender;
		}else{
			$gender = $user_data['gender'];
		}
		
/* 		$fit = get_field( 'fit', $user_id );
		if($fit){
			$fit = $fit;
		}else{
			$fit = $user_data['fit'];
		} */
		$fit_style_arr = array('Measurement Style 1','Measurement Style 2','Measurement Style 3');

		$fit = get_field( 'fit',$user_id );
		$fit_arr_e = explode('|', $fit);
		$fit_arr_f = array_filter($fit_arr_e, function($value) { return $value !== ''; });
		$fit_arr = array_reverse($fit_arr_f,true);
		/* ?><pre><?php print_r($fit_arr); ?></pre><?php */

		if($fit_arr){
			$fit_arr = $fit_arr;
			$fit_style_arr=array_diff($fit_style_arr,$fit_arr);
			/* ?><pre><?php print_r('1'); ?></pre><?php */
		}elseif($user_data['fit']){
			$fit_arr = $user_data['fit'];
			$fit_style_arr=array_diff($fit_style_arr,$fit_arr);
			/* ?><pre><?php print_r('2'); ?></pre><?php */
		}else{
/* 			?><pre><?php print_r('3'); ?></pre><?php*/			
		$fit_style_arr = array('Measurement Style 1','Measurement Style 2','Measurement Style 3');
		}
		
		$length = get_field( 'length',$user_id );
		$length_arr_e = explode('|', $length);
		$length_arr_f = array_filter($length_arr_e, function($value) { return $value !== ''; });
		$length_arr = array_reverse($length_arr_f,true);
		if($length_arr){
			$length_arr = $length_arr;
		}else{
			$length_arr = $user_data['length'];
		}
		$length_arr = is_array($length_arr) ? $length_arr : array($length_arr);
		$waist = get_field( 'waist',$user_id );
		$waist_arr_e = explode('|', $waist);

		$waist_arr_f = array_filter($waist_arr_e, function($value) { return $value !== ''; });
		$waist_arr = array_reverse($waist_arr_f,true);
		if($waist_arr){
			//echo 'sfd';
			$waist_arr = $waist_arr;
		}else{
			//echo 'sf3232d';
			$waist_arr = $user_data['waist'];
		}
		$waist_arr = is_array($waist_arr) ? $waist_arr : array($waist_arr);

		$seat = get_field( 'seat',$user_id );
		$seat_arr_e = explode('|', $seat);
		$seat_arr_f = array_filter($seat_arr_e, function($value) { return $value !== ''; });
		$seat_arr = array_reverse($seat_arr_f,true);
		if($seat_arr){
			$seat_arr = $seat_arr;
		}else{
			$seat_arr = $user_data['seat'];
		}
		$seat_arr = is_array($seat_arr) ? $seat_arr : array($seat_arr);

		$front_rise = get_field( 'front_rise',$user_id );
		$front_rise_arr_e = explode('|', $front_rise);
		$front_rise_arr_f = array_filter($front_rise_arr_e, function($value) { return $value !== ''; });
		$front_rise_arr = array_reverse($front_rise_arr_f,true);
		if($front_rise_arr){
			$front_rise_arr = $front_rise_arr;
		}else{
			$front_rise_arr = $user_data['front_rise'];
		}
		$front_rise_arr = is_array($front_rise_arr) ? $front_rise_arr : array($front_rise_arr);
		$thighs = get_field( 'thighs',$user_id );
		$thighs_arr_e = explode('|', $thighs);
		$thighs_arr_f = array_filter($thighs_arr_e, function($value) { return $value !== ''; });
		$thighs_arr = array_reverse($thighs_arr_f,true);
		if($thighs_arr){
			$thighs_arr = $thighs_arr;
		}else{
			$thighs_arr = $user_data['thighs'];
		}
		$thighs_arr = is_array($thighs_arr) ? $thighs_arr : array($thighs_arr);
		
		$leg_opening = get_field( 'leg_opening',$user_id );
		$leg_opening_arr_e = explode('|', $leg_opening);
		$leg_opening_arr_f = array_filter($leg_opening_arr_e, function($value) { return $value !== ''; });
		$leg_opening_arr = array_reverse($leg_opening_arr_f,true);
		if($leg_opening_arr){
			$leg_opening_arr = $leg_opening_arr;
		}else{
			$leg_opening_arr = $user_data['leg_opening'];
		}
		$leg_opening_arr = is_array($leg_opening_arr) ? $leg_opening_arr : array($leg_opening_arr);
		$back_rise = get_field( 'back_rise',$user_id );
		$back_rise_arr_e = explode('|', $back_rise);
		$back_rise_arr_f = array_filter($back_rise_arr_e, function($value) { return $value !== ''; });
		$back_rise_arr = array_reverse($back_rise_arr_f,true);
		if($back_rise_arr){
			$back_rise_arr = $back_rise_arr;
		}else{
			$back_rise_arr = $user_data['back_rise'];
		}
		$back_rise_arr = is_array($back_rise_arr) ? $back_rise_arr : array($back_rise_arr);
		$knee = get_field( 'knee',$user_id );
		$knee_arr_e = explode('|', $knee);
		$knee_arr_f = array_filter($knee_arr_e, function($value) { return $value !== ''; });
		$knee_arr = array_reverse($knee_arr_f,true);
		if($knee_arr){
			$knee_arr = $knee_arr;
		}else{
			$knee_arr = $user_data['knee'];
		}
		$knee_arr = is_array($knee_arr) ? $knee_arr : array($knee_arr);
		$profile_name = get_field( 'profile_name',$user_id );
		if($profile_name){
			$profile_name = $profile_name;
		}else{
			$profile_name = $user_data['profile_name'];
		}
		
		//$success_msg = get_field( 'success_msg',$user_id );
		/* if($success_msg == 'yes'){
			
		}elseif($success_msg == 'no'){

		} */
		/* ?><pre><?php print_r(count($fit_style_arr)); ?></pre><?php
		$fit_style_count = count($fit_style_arr); ?></pre><?php */
		

	?>

						<form role="form" action="" method="post" id="addmenu" method="post">
								<div class="form-group">
									<label class="control-label" for="address">Full Name:</label>
									<input placeholder="Enter Name" type="text" name="user_name" size="20" maxlength="32" class="inches" required="yes" message="Please Enter Name(For)" requiredfield="yes" value="<?php echo ($user_name); ?>" id="attrib-1-0" textoptionid="1">
								</div>
								<div class="form-group">
								<label class="control-label" for="address">Gender:</label>
									<select name="id[gender]" id="attrib-2" validate="text" message="Please Select Gender" required="yes" default="--Select--">
									<?php 
									if($gender){
										?>
										<option value="<?php echo $gender; ?>" selected><?php echo $gender; ?></option>
										<?php
									}else{
										?>
										<option value="">--Select--</option>
									<?php
									}
									?>
									  
									  <option value="Male">Male</option>
									  <option value="Female">Female</option>
									</select>
								</div>	
								<?php
								if($fit_style_count >= 1){
								?>								
								<div class="form-group">
									<div class="row">
									<div class="col-md-12"><label class="control-label" for="address">Add Measurement Style</label><span> ( You can add Maximum 3 measurnents )</span></div>
										<div class="col-md-4">
										<select name="id[fit_style]" id="fit_style" required>
										<!--<option value="">--Select Fit Style--</option> -->
										<?php 
										foreach($fit_style_arr as $fit_style){ ?>
										  <option value="<?php echo $fit_style; ?>"><?php echo $fit_style; ?></option>
										<?php } ?>
										</select>
										</div>
										<div class="col-md-3">
											<input type="button" id="add_style" value="Click to Start" class="submit_measurement btn btn-primary btn-block">
										</div>
									</div>
								</div>
								<?php 
								}
								if($fit_arr){ ?>
								
								<div class="size_div">
									<div class="form-group">
										<div class="row fitstyleslist">
											<?php 
											if($fit_arr){ ?>
												<label class="control-label" for="address">Fit Styles List</label>

											<?php
														foreach($fit_arr as $fit){
														?>
															 <div class="field-wrapper measurement-field">
																<label><?php echo $fit; ?></label>
																<input class="hide_me fit_input" type="text" name="id[fit][]" placeholder="Firmanavn / Oppdragsgiver" value="<?php echo $fit; ?>" required1="" >
															</div>
													<?php
														} ?>
										
												<?php } ?>
										</div>
									</div>
									<div class="form-group">
										
										<div class="row">
											<?php 
											if($length_arr[0]){
												?>
												<label class="control-label" for="address">Length (CM)</label>
												<?php
													foreach($length_arr as $length){
													?>
														 <div class="field-wrapper measurement-field">
															<input type="number" min="0" name="id[length][]" placeholder="Length (CM)" value="<?php echo $length; ?>" required="" >
														</div>
												<?php
													} 
											}
												?>
										</div>
									</div>	
							
								<div class="form-group">
									
									
									<div class="row">
											<?php 
											if($waist_arr[0]){
												?><label class="attribsInput" for="attrib-5-0">Waist (CM)</label> 
												<?php 
													foreach($waist_arr as $waist){ ?>
								   <div class="field-wrapper measurement-field">
								   
									  <input type="number" min="0" name="id[waist][]" placeholder="Waist (CM)" value="<?php echo $waist; ?>" required>  
									 
								   </div>
								   <?php
													} 
											}
												?>
									</div>
								</div>
								<div class="form-group">
									
									
									<div class="row">
											<?php 
											if($seat_arr[0]){
												
												?><label class="attribsInput" for="attrib-6-0">Seat (CM)</label>
												<?php
												foreach($seat_arr as $seat){ ?>
												<div class="field-wrapper measurement-field ">
													<input type="number" min="0" name="id[seat][]" size="5" maxlength="5" class="inches" validate="float" message="Please enter Seat Measurements" required="yes" max="85" decimalpoint="25" equalmorethan="9" value="<?php echo $seat; ?>" id="attrib-6-0" textoptionid="6" required>  
											   </div>
												<?php
												} 
											}
												?>
									</div>
								</div>
							   <div class="form-group">
									 
									<div class="row">
											<?php 
											if($front_rise_arr[0]){
												?>
												 <label class="attribsInput" for="attrib-23-0">Front Rise (CM)</label> 
												<?php
												foreach($front_rise_arr as $front_rise){ ?>
												 <div class="field-wrapper measurement-field ">
								 
									  <input type="number" min="0" name="id[front_rise][]" size="5" maxlength="5" class="inches" validate="float" message="Please enter Front Rise Measurements" required="yes" min="3" max="18" decimalpoint="25" value="<?php echo $front_rise; ?>" id="attrib-23-0" textoptionid="23" required>  
									  
								   </div>
												<?php
												} 
											} 
												?>
									</div>
								</div>
							   <div class="form-group">
									  
									<div class="row">
											<?php 
											if($back_rise_arr[0]){
												?> <label class="attribsInput" for="attrib-23-0">Back Rise (CM)</label>
												<?php
												foreach($back_rise_arr as $back_rise){ ?>
												
												 <div class="field-wrapper measurement-field ">
									  <input type="number" min="0" name="id[back_rise][]" size="5" maxlength="5" class="inches" validate="float" message="Please enter Back Rise Measurements" value="<?php echo ($back_rise) ? $back_rise : ''; ?>" id="attrib-42-0" textoptionid="42" required>  
									  
								   </div>
								   <?php
												} 
											}
												?>
									</div>
								</div>
								   <div class="form-group">
									 
									<div class="row">
											<?php 
											if($thighs_arr[0]){
												?>  <label class="attribsInput" for="attrib-23-0">Thighs (CM)</label>
												<?php
												foreach($thighs_arr as $thighs){ ?>
												  <div class="field-wrapper measurement-field  custom-fit-option" data-name="<label class=&quot;attribsInput&quot; for=&quot;attrib-7-0&quot;>Thighs</label>" data-description="" data-image-base-url="includes/templates/studiojeans/images/attributes/thighs.jpg" data-num-images="1">
									  
									  <input type="number" min="0" name="id[thighs][]" size="5" maxlength="5" class="inches" validate="float" message="Please enter Thigh Measurements" required="yes" max="50" decimalpoint="25" checkdependent="checkDependent('12', '40', '((DependValue/4) * 2)', 'Enter Seat First.', 'According to your Seat, Finished Thighs should be minimum DependValue.');" value="<?php echo ($thighs) ? $thighs : ''; ?>" id="attrib-7-0" textoptionid="7" required>  
									  
								   </div>  
								   <?php
												} 
											} 
												?>
									</div>
								</div>
								<div class="form-group">
									 
									<div class="row">
											<?php 
											if($knee_arr[0]){
												?> <label class="attribsInput" for="attrib-23-0">Knee (CM)</label> 
												<?php
												foreach($knee_arr as $knee){ ?>
												 <div class="field-wrapper measurement-field  custom-fit-option" data-name="<label class=&quot;attribsInput&quot; for=&quot;attrib-7-0&quot;>Thighs</label>" data-description="" data-image-base-url="includes/templates/studiojeans/images/attributes/thighs.jpg" data-num-images="1">
									  
									  <input type="number" min="0" name="id[knee][]" size="5" maxlength="5" class="inches" validate="float" message="Please enter Thigh Measurements" required="yes" max="50" decimalpoint="25" checkdependent="checkDependent('12', '40', '((DependValue/4) * 2)', 'Enter Seat First.', 'According to your Seat, Finished Thighs should be minimum DependValue.');" value="<?php echo ($knee) ? $knee : ''; ?>" id="attrib-7-0" textoptionid="7" required>  
									  
								   </div>
												<?php
												} 
											} 
									?>
									</div>
								</div>
								  <div class="form-group">
								  
									 
									<div class="row">
											<?php 
											if($leg_opening_arr[0]){
												?> <label class="attribsInput" for="attrib-23-0">Leg Opening (CM)</label> 
												<?php
												foreach($leg_opening_arr as $leg_opening){ ?>
												 <div class="field-wrapper measurement-field ">
									  <input type="number" min="0" name="id[leg_opening][]" size="5" maxlength="5" class="inches" validate="float" min="9" max="40" decimalpoint="25" value="<?php echo ($leg_opening) ? $leg_opening : ''; ?>" id="attrib-24-0" textoptionid="24" required>  
									</div> 
											<?php
												} 
											} 
									?>
									</div>
								</div>
								    
								</div>
								<?php }else{
									echo '<div class="size_div"></div>';
								} ?>
								 <div class="field-wrapper measurement-field">
		<input type="submit" name="submit_measurement" value="Save Measurement" class="submit_measurement btn btn-primary btn-block">
	 </div>
							   <div class="field-wrapper measurement-field">
		<a href="<?php echo site_url().'/shop/';?>"><input type="button" value="Go to Shop" class="submit_measurement btn btn-primary btn-block"></a>
	 </div>
	 <div class="field-wrapper measurement-field">
		<a href="<?php echo site_url().'/my-profile/edit-account/';?>"><input type="button" value="My profile" class="submit_measurement btn btn-primary btn-block"></a>
	 </div>

								<?
/*    <div class="field-wrapper measurement-field">
    <b><label class="attribsInput" for="attrib-24-0">Measurement profile name</label></b> 
      <input type="number" min="0" name="id[profile_name]" class="inches" validate="float" min="9" max="40" decimalpoint="25" value="<?php echo ($profile_name) ? $profile_name : ''; ?>" id="attrib-24-0" textoptionid="24">  
     
   </div> */
  ?>

	</form>
	<script>
	jQuery(document).ready(function(){
	jQuery("#add_style").click(function(e) {
e.preventDefault();
console.log('asdas');
var fit_style = jQuery( "#fit_style" ).val();
 jQuery("#fit_style option[value='" + fit_style + "']").attr('disabled','disabled');
 jQuery(".size_div").prepend("<div class='form-group' id='style_list'><div class='row'><div class='col-md-12'><b><label class='attribsInput' for='attrib-8-0'>Fit Style</label></b><input type='text' name='id[fit][]' value='"+fit_style+"' class='inches' required></div></div><div class='row'><div class='col-md-3'><b><label for='phone_number1'>Lenght (CM)</label></b><input type='number' min='0' name='id[length][]' placeholder='CM' required></div><div class='col-md-3'><b><label for='phone_number1'>Waist (CM)</label></b><input type='number' min='0' name='id[waist][]' placeholder=' CM' required></div><div class='col-md-3'><b><label for='phone_number1'>Seat (CM)</label></b><input type='number' min='0' name='id[seat][]' placeholder='CM' required></div><div class='col-md-3'><b><label for='phone_number1'>Front Rise (CM)</label></b><input type='number' min='0' name='id[front_rise][]' placeholder='CM' required></div></div><div class='row'><div class='col-md-3'><b><label for='phone_number1'>Back Rise (CM)</label></b><input type='number' min='0' name='id[back_rise][]' placeholder='CM' required></div><div class='col-md-3'><b><label for='phone_number1'>Thighs (CM)</label></b><input type='number' min='0' name='id[thighs][]' placeholder='CM' required></div><div class='col-md-3'><b><label for='phone_number1'>Knee (CM)</label></b><input type='number' min='0' name='id[knee][]' placeholder='CM' required></div><div class='col-md-3'><b><label for='phone_number1'>Leg Opening (CM)</label></b><input type='number' min='0' name='id[leg_opening][]' placeholder='CM' required></div></div></div>");  
});
});
	</script>
	<?php
	}else{
		//site_url();
		echo 'Please <a href="'.site_url().'/login/">Login/Register</a> to add your measurement';
	}
}
function geo_my_wp_script_back_css() {

}

function geo_my_wp_script_front_css() {
		/* CSS */
        wp_register_style('geo_my_wp_style', plugins_url('css/geo_my_wp.css',__FILE__));
        wp_enqueue_style('geo_my_wp_style');
}

		add_action( 'wp_ajax_my_ajax_rt', 'my_ajax_rt' );
		add_action( 'wp_ajax_nopriv_my_ajax_rt', 'my_ajax_rt' );
function my_ajax_rt() {
	
}

function geo_my_wp_script_back_js() {
	
}



function geo_my_wp_script_front_js() {
 	
			   
        wp_register_script('geo_my_wp_script', plugins_url('js/geo_my_wp.js', __FILE__ ),array('jquery'));
        wp_enqueue_script('geo_my_wp_script');

}


function my_login_redirect( $redirect_to, $request, $user ) {
    //is there a user to check?

	$interested_course_type = get_user_meta( $user->ID , 'interested_course_type', true);
	?><pre><?php print_r($interested_course_type); ?> </pre> <?php
    if ($interested_course_type) {
        //check for admins
		 return site_url() . "/candidate-ranks/";
    } else {
		return site_url() . "/candidate-ranks/";
    }
	return site_url() . "/candidate-ranks/";;
}
 
//add_filter( 'login_redirect', 'my_login_redirect', 1, 3 );
add_action('wp_enqueue_scripts', 'wp_profile_script_front_css');
//add_action('wp_enqueue_scripts', 'wp_profile_script_front_js');
//add_action('admin_enqueue_scripts', 'wp_profile_script_back_css');
add_action('init', 'wp_astro_init2');
function get_variable_items_contents($type, $options, $args, $saved_attribute = array()) {

        $product = $args['product'];
        $attribute = $args['attribute'];
        $data = '';
?><pre><?php print_r($product); ?></pre><?php
?><pre><?php print_r($attribute); ?></pre><?php

        if (!empty($options)) {
            if ($product && taxonomy_exists($attribute)) {
                $terms = wc_get_product_terms($product->get_id(), $attribute, array('fields' => 'all'));
                $name = uniqid(wc_variation_attribute_name($attribute));
                foreach ($terms as $term) {
                    if (in_array($term->slug, $options)) {
                        $selected_class = (sanitize_title($args['selected']) == $term->slug) ? 'selected' : '';
                        $tooltip = trim(apply_filters('rtwpvs_variable_item_tooltip', $term->name, $term, $args));

                        $tooltip_html_attr = !empty($tooltip) ? sprintf('data-rtwpvs-tooltip="%s"', esc_attr($tooltip)) : '';

                        if (wp_is_mobile()) {
                            $tooltip_html_attr .= !empty($tooltip) ? ' tabindex="2"' : '';
                        }

                        $data .= sprintf('<div %1$s class="rtwpvs-term rtwpvs-%2$s-term %2$s-variable-term-%3$s %4$s" data-term="%3$s">', $tooltip_html_attr, esc_attr($type), esc_attr($term->slug), esc_attr($selected_class));

                        switch ($type):
                            case 'color':
                                $color = sanitize_hex_color(get_term_meta($term->term_id, 'product_attribute_color', true));
                                $data .= sprintf('<span class="rtwpvs-term-span rtwpvs-term-span-%s" style="background-color:%s;"></span>', esc_attr($type), esc_attr($color));
                                break;

                            case 'image':
                                $attachment_id = absint(get_term_meta($term->term_id, 'product_attribute_image', true));
                                $image_size = rtwpvs()->get_option('attribute_image_size');
                                $image_url = wp_get_attachment_image_url($attachment_id, apply_filters('rtwpvs_product_attribute_image_size', $image_size));
                                $data .= sprintf('<img alt="%s" src="%s" />', esc_attr($term->name), esc_url($image_url));
                                break;

                            case 'button':
                                $data .= sprintf('<span class="rtwpvs-term-span rtwpvs-term-span-%s">%s</span>', esc_attr($type), esc_html($term->name));
                                break;

                            case 'radio':
                                $id = uniqid($term->slug);
                                $data .= sprintf('<input name="%1$s" id="%2$s" class="rtwpvs-radio-button-term" %3$s  type="radio" value="%4$s" data-term="%4$s" /><label for="%2$s">%5$s</label>', $name, $id, checked(sanitize_title($args['selected']), $term->slug, false), esc_attr($term->slug), esc_html($term->name));
                                break;

                            default:
                                $data .= apply_filters('rtwpvs_variable_default_item_content', '', $term, $args, $saved_attribute);
                                break;
                        endswitch;
                        $data .= '</div>';
                    }
                }
            }
        }

        $contents = apply_filters('rtwpvs_variable_term', $data, $type, $options, $args, $saved_attribute);

        $attribute = $args['attribute'];

        $css_classes = apply_filters('rtwpvs_variable_terms_wrapper_class', array("{$type}-variable-wrapper"), $type, $args, $saved_attribute);

        $data = sprintf('<div class="rtwpvs-terms-wrapper %s" data-attribute_name="%s">%s</div>', trim(implode(' ', array_unique($css_classes))), esc_attr(wc_variation_attribute_name($attribute)), $contents);

        return apply_filters('rtwpvs_variable_items_wrapper', $data, $contents, $type, $args, $saved_attribute);
    }

function wp_astro_init2() {
	add_shortcode('edit_profile', 'wp_profile_form');
	add_shortcode('profile_header', 'wp_profile_header');
	add_shortcode('carrer_by_level_tex', 'carrer_by_level');
	add_shortcode('show_carrer_by_level', 'carrer_by_level_fnc');
	//add_shortcode('show_carrer_by_level', 'carrer_by_level_fnc');
}
function carrer_by_level_fnc($atts) {

			$args = array(
				'post_type' => 'career',
				'post_status' => 'publish',
				'posts_per_page' => -1
			);
			$postslist = get_posts( $args );
			echo '<div class="wpb_text_column "><div class="row">';
			if($postslist){
			
				foreach($postslist as $post){
					echo '<div class="col-md-6" href="'.get_permalink($post->ID).'"><h1>'.$post->post_title.'</h1></div><div class="col-md-6 career-btn">';
					echo do_shortcode('[show_gd_mylist_btn add_icon="fa fa-user-plus" remove_icon="fa fa-user-plus" add_label="Follow" remove_label="Un Follow" item_id="'.$post->ID.'"]');
					echo '</div><div class="col-md-12 career-level"><p>'.$post->post_content.'</p>';
					$post_meta = get_post_meta($post->ID);
					$basic_career_courses = $post_meta['basic_career_courses'];
					if($basic_career_courses){
						echo'<h6 class="course_list">Basic Courses List</h5>';
						foreach($basic_career_courses as $career_course){
							echo '<a href="'.get_permalink($career_course).'"><p>'.get_the_title($career_course).'</p></a>';
						}
					}
					$intermediate_career_courses = $post_meta['intermediate_career_courses'];
					if($intermediate_career_courses){
						echo'<h6 class="course_list">Intermediate Courses List</h5>';
						foreach($intermediate_career_courses as $career_course){
							echo '<a href="'.get_permalink($career_course).'"><p>'.get_the_title($career_course).'</p></a>';
						}
					}
					$advance_career_courses = $post_meta['advance_career_courses'];
					if($advance_career_courses){
						echo'<h6 class="course_list">Advance Courses List</h5>';
						foreach($advance_career_courses as $career_course){
							echo '<a href="'.get_permalink($career_course).'"><p>'.get_the_title($career_course).'</p></a>';
						}
					}
					echo '</div>';
				}
			}else{
				echo 'No Carrer found';
			}
			echo '</div></div>';
}

function wp_profile_script_back_css() {

}

function wp_profile_script_front_css() {
		/* CSS */
        wp_register_style('wp_profile_style', plugins_url('css/wp_profile.css',__FILE__),array(),time());
        wp_enqueue_style('wp_profile_style');
        wp_register_style('bootstrap_style', plugins_url('css/bootstrap.min.css',__FILE__));
        wp_enqueue_style('bootstrap_style');
}

/* 		add_action( 'wp_ajax_my_ajax_rt', 'my_ajax_rt' );
		add_action( 'wp_ajax_nopriv_my_ajax_rt', 'my_ajax_rt' );
function my_ajax_rt() {
	
} */

function wp_profile_script_back_js() {
	
}



function wp_profile_script_front_js() {	   
        wp_register_script('wp_profile_script', plugins_url('js/wp_profile.js', __FILE__ ),array('jquery'));
        wp_enqueue_script('wp_profile_script');
        wp_register_script('bootstrap_script', plugins_url('js/bootstrap.min.js', __FILE__ ),array('jquery'));
        wp_enqueue_script('bootstrap_script');

}
