<?php
/*
Plugin Name: Geo My Wp custom form
Description: Geo My Wp custom form
Version: 1.0.0
Plugin URI: https://www.fiverr.com/wp_right  
Author: LogicsBuffer
Author URI: http://logicsbuffer.com/
*/

add_action('wp_enqueue_scripts', 'geo_my_wp_script_front_css');
add_action('wp_enqueue_scripts', 'geo_my_wp_script_front_js');
add_action('admin_enqueue_scripts', 'geo_my_wp_script_back_css');
add_action('init', 'wp_astro_init');
function wp_astro_init() {
   add_shortcode('show_geomywp_form', 'wp_geo_my_wp_form');
}
function wp_geo_my_wp_form($atts) {
	if(isset($_POST['submit_measurement'])){
		$user_id = get_current_user_id();
	/* 	 [id] => Array
        (
            [user_name] => rijohet
            [gender] => 7
            [fit] => 6
            [length] => Rerum
            [waist] => Omnis
            [seat] => Commo
            [front_rise] => Proid
            [back_rise] => Et au
            [thighs] => Ad nu
            [leg_opening] => Bevis Ross
            [profile_name] => Zelenia Rosario
        ) */
		$user_data = $_POST['id'];
		/* ?><pre><?php print_r($user_data); ?> </pre><?php */

		$user_name = $user_data['user_name'];
		$gender = $user_data['gender'];
		$fit = $user_data['fit'];
		$length = $user_data['length'];
		$waist = $user_data['waist'];
		$seat = $user_data['seat'];
		$front_rise = $user_data['front_rise'];
		$back_rise = $user_data['back_rise'];
		$thighs = $user_data['thighs'];
		$leg_opening = $user_data['leg_opening'];
		$profile_name = $user_data['profile_name'];
		$user_id = 'user_'.$user_id;
		update_field( 'gender', $gender, $user_id );
		update_field( 'fit', $fit, $user_id );
		update_field( 'length', $length, $user_id );
		update_field( 'waist', $waist, $user_id );
		update_field( 'seat', $seat, $user_id );
		update_field( 'front_rise', $front_rise, $user_id );
		update_field( 'thighs', $thighs, $user_id );
		update_field( 'leg_opening', $leg_opening, $user_id );
		update_field( 'back_rise', $back_rise, $user_id );

	}
	if(is_user_logged_in()){
	?>

<form role="form" action="" method="post" id="addmenu" method="post">
								<div class="form-group">
								<div class="alert alert-success">
								</div>								
								</div>
								<div class="form-group">
									<label class="control-label" for="address">Enter Name:</label>
									<input placeholder="Enter Name" type="text" name="id[user_name]" size="20" maxlength="32" class="inches" required="yes" message="Please Enter Name(For)" requiredfield="yes" value="" id="attrib-1-0" textoptionid="1">
								</div>
								<div class="form-group">
								<label class="control-label" for="address">Gender:</label>
									<select name="id[gender]" id="attrib-2" validate="text" message="Please Select Gender" required="yes" default="--Select--">
									  <option value="1496">--Select--</option>
									  <option value="Male">Male</option>
									  <option value="Female">Female</option>
									</select>
								</div>		
								<div class="form-group">
								<label class="control-label" for="address">Fit:</label>
									<select name="id[fit]" id="attrib-3">
									  <option value="335">--Select--</option>
									  <option value="3236">Fix</option>
									  <option value="Classic Comfort Fit">Classic Comfort Fit</option>
									  <option value="Straight Fit">Straight Fit</option>
									  <option value="Boot Cut">Boot Cut</option>
									  <option value="Semi Baggy">Semi Baggy</option>
									  <option value="Baggy">Baggy</option>
									  <option value="Slimmer Tapering">Slimmer Tapering</option>
									  <option value="Slimmer Straight">Slimmer Straight</option>
									  <option value="Slimmer Bootcut">Slimmer Bootcut</option>
									</select>
								</div>
								<div class="field-wrapper measurement-field " data-name="<label class=&quot;attribsInput&quot; for=&quot;attrib-8-0&quot;>Length</label>" data-description="<ol class=&quot;tips&quot;><li>Measure the length from the waist to the floor or the length you want, you should measure the length while wearing your shoes.</li><li>To get more accurate measurements you may also measure the outer length of your existing jeans and input the length which you find greater</li><li><font color=&quot;red&quot;>Please note this is outer length and NOT inseam.</font></li></ol>" data-image-base-url="includes/templates/studiojeans/images/attributes/length.jpg" data-num-images="1">
      <b><label class="attribsInput" for="attrib-8-0">Length</label></b> <span class="guesstimated">Guesstimated</span>
	  <input type="text" name="id[length]" size="5" maxlength="5" class="inches" message="WARNING: Please note for length we only accept outer length and NOT Inseam$$newline$$Click OK and Submit if you are sure or click Cancel and edit" required="yes" min="35" max="60" once="1" bareminimum="22" bareminmsg="The Outer length" value="" id="attrib-8-0" textoptionid="8">  
      
   </div>
   <div class="field-wrapper measurement-field " data-name="<label class=&quot;attribsInput&quot; for=&quot;attrib-5-0&quot;>Waist</label>" data-description="<ol class=&quot;tips&quot;><li>Measure the waist area where you want to wear your Jeans. Make sure the tape is snug, but not pulling tightly, and not too loose.</li><li><font color=&quot;red&quot;>Do NOT mention your label size</font>, measure your waist according to the instructions above, readymade jeans waist measurement is always more than their label sizes and vary from brand to brand</li></ol>" data-image-base-url="includes/templates/studiojeans/images/attributes/waist.jpg" data-num-images="1">
    <b><label class="attribsInput" for="attrib-5-0">Waist</label></b> <span class="guesstimated">Guesstimated</span>
      <input type="text" name="id[waist]" size="5" maxlength="5" class="inches" validate="float" message="Please enter Waist Measurements" required="yes" min="22" max="80" decimalpoint="25" value="" id="attrib-5-0" textoptionid="5">  
     
   </div>
   <div class="field-wrapper measurement-field " data-name="<label class=&quot;attribsInput&quot; for=&quot;attrib-6-0&quot;>Seat</label>" data-description="<ol class=&quot;tips&quot;><li>Seat is the widest part of your hips. Keep the measuring tape here and the area which gives the maximum measurement is the &quot;SEAT&quot; required.</li></ol>" data-image-base-url="includes/templates/studiojeans/images/attributes/seat.jpg" data-num-images="1">
 <b><label class="attribsInput" for="attrib-6-0">Seat</label></b>
   <span class="guesstimated">Guesstimated</span>
      <input type="text" name="id[seat]" size="5" maxlength="5" class="inches" validate="float" message="Please enter Seat Measurements" required="yes" max="85" decimalpoint="25" equalmorethan="9" value="" id="attrib-6-0" textoptionid="6">  
       
   </div>
   <div class="field-wrapper measurement-field " data-name="<label class=&quot;attribsInput&quot; for=&quot;attrib-23-0&quot;>Front Rise</label>" data-description="<br>Front Rise is the distance from the middle of the crotch where the seams meet (right between your legs) to the top of the waistband. <br><br> <ol class=&quot;tips&quot;><li>Measure from the crotch where the inseam meets the front and back seams to the top of the waistband.</li><li> Measure with the front laid flat, pull the area to the fullest.</li><li>You may increase or decrease this measurement based on personal preference.</ol>" data-image-base-url="includes/templates/studiojeans/images/attributes/front_rise.jpg" data-num-images="1">
  <b> <label class="attribsInput" for="attrib-23-0">Front Rise</label></b> <span class="guesstimated">Guesstimated</span>
      <input type="text" name="id[front_rise]" size="5" maxlength="5" class="inches" validate="float" message="Please enter Front Rise Measurements" required="yes" min="3" max="18" decimalpoint="25" value="" id="attrib-23-0" textoptionid="23">  
      
   </div>
   <div class="field-wrapper measurement-field " data-name="<label class=&quot;attribsInput&quot; for=&quot;attrib-42-0&quot;>Back Rise</label>" data-description="<ol class=&quot;tips&quot;><font color=&quot;red&quot;>Optional</font><li>Measure from the crotch where the inseam meets the front and back seams to the top of the waistband on the rear.</li><li>Measure with the back laid flat, pull the area to the fullest.</li><li>We will increase/decrease the back rise incase we feel that it seems a bit off or do not come in the range of your other measurements.</li></ol>" data-image-base-url="includes/templates/studiojeans/images/attributes/back_rise.jpg" data-num-images="1">
  <b> <label class="attribsInput" for="attrib-42-0">Back Rise</label></b> <span class="guesstimated">Guesstimated</span>
      <input type="text" name="id[back_rise]" size="5" maxlength="5" class="inches" validate="float" message="Please enter Back Rise Measurements" value="" id="attrib-42-0" textoptionid="42">  
      
   </div>
   <div class="field-wrapper measurement-field  custom-fit-option" data-name="<label class=&quot;attribsInput&quot; for=&quot;attrib-7-0&quot;>Thighs</label>" data-description="" data-image-base-url="includes/templates/studiojeans/images/attributes/thighs.jpg" data-num-images="1">
      
	  <b><label class="attribsInput" for="attrib-7-0">Thighs</label></b> <span class="guesstimated">Guesstimated</span>
      <input type="text" name="id[thighs]" size="5" maxlength="5" class="inches" validate="float" message="Please enter Thigh Measurements" required="yes" max="50" decimalpoint="25" checkdependent="checkDependent('12', '40', '((DependValue/4) * 2)', 'Enter Seat First.', 'According to your Seat, Finished Thighs should be minimum DependValue.');" value="" id="attrib-7-0" textoptionid="7" >  
      
   </div>
   <div class="field-wrapper measurement-field " data-name="<label class=&quot;attribsInput&quot; for=&quot;attrib-24-0&quot;>Leg Opening</label>" data-description="<ol class=&quot;tips&quot;><li>The leg opening measurement determines how wide or narrow the bottom opening of the leg is.</li><li>Measure the leg bottom opening across the jeans , <font color=&quot;red&quot;>multiply this measurement by 2</font>.</li><li>You may increase or decrease this number based on personal preference with regards to style. </li> <br><Br> Tip: Do not forget to multiply this measurement by 2 after measuring as per the picture as we need the circumference.</ol>" data-image-base-url="includes/templates/studiojeans/images/attributes/leg_opening.jpg" data-num-images="1">
    <b><label class="attribsInput" for="attrib-24-0">Leg Opening</label></b> <span class="guesstimated">Guesstimated</span>
      <input type="text" name="id[leg_opening]" size="5" maxlength="5" class="inches" validate="float" min="9" max="40" decimalpoint="25" value="" id="attrib-24-0" textoptionid="24">  
     
   </div>   
   <div class="field-wrapper measurement-field " data-name="<label class=&quot;attribsInput&quot; for=&quot;attrib-24-0&quot;>Leg Opening</label>" data-description="<ol class=&quot;tips&quot;><li>The leg opening measurement determines how wide or narrow the bottom opening of the leg is.</li><li>Measure the leg bottom opening across the jeans , <font color=&quot;red&quot;>multiply this measurement by 2</font>.</li><li>You may increase or decrease this number based on personal preference with regards to style. </li> <br><Br> Tip: Do not forget to multiply this measurement by 2 after measuring as per the picture as we need the circumference.</ol>" data-image-base-url="includes/templates/studiojeans/images/attributes/leg_opening.jpg" data-num-images="1">
    <b><label class="attribsInput" for="attrib-24-0">Measurement profile name</label></b> <span class="guesstimated">Guesstimated</span>
      <input type="text" name="id[profile_name]" size="5" maxlength="5" class="inches" validate="float" min="9" max="40" decimalpoint="25" value="" id="attrib-24-0" textoptionid="24">  
     
   </div>
	<input type="submit" name="submit_measurement" value="Submit Measurement" class="btn btn-primary btn-block">
	</form>
	<?php
	}
}
function geo_my_wp_script_back_css() {

}

function geo_my_wp_script_front_css() {
		/* CSS */
        wp_register_style('geo_my_wp_style', plugins_url('css/geo_my_wp.css',__FILE__));
        wp_enqueue_style('geo_my_wp_style');
}

		add_action( 'wp_ajax_my_ajax_rt', 'my_ajax_rt' );
		add_action( 'wp_ajax_nopriv_my_ajax_rt', 'my_ajax_rt' );
function my_ajax_rt() {
	
}

function geo_my_wp_script_back_js() {
	
}



function geo_my_wp_script_front_js() {
 	
			   
        wp_register_script('geo_my_wp_script', plugins_url('js/geo_my_wp.js', __FILE__ ),array('jquery'));
        wp_enqueue_script('geo_my_wp_script');

}
