<?php

add_action( 'wp_enqueue_scripts', 'goodtailor_child_enqueue_styles' );

function goodtailor_child_enqueue_styles() {
	wp_enqueue_style( 'goodtailor-child-style', get_stylesheet_uri(), array( 'goodtailor-style' ), '1.0.0' );
}
/* WooCommerce Add To Cart Text */

add_filter('woocommerce_product_single_add_to_cart_text', 'woocommerce_custom_add_to_cart_text');
 
function woocommerce_custom_add_to_cart_text() {
return __('Add to the basket', 'woocommerce');
}
