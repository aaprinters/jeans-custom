<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @package GoodTailor
 */

?><!DOCTYPE html>
<html class="no-js" <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<?php
	if ( function_exists( 'wp_body_open' ) ) {
		wp_body_open();
	}
	?>

	<a class="skip-link screen-reader-text" href="#site-navigation"><?php esc_html_e( 'Skip to navigation', 'goodtailor' ); ?></a>
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'goodtailor' ); ?></a>

	<header id="masthead" <?php goodtailor_header_class(); ?>>
		<?php get_template_part( 'partials/header/top-bar' ); ?>

		<div class="site-header-navbar">
			<div class="container">
				<?php get_template_part( 'partials/header/site-branding' ); ?>
				<?php get_template_part( 'partials/header/primary-navigation' ); ?>
				<?php get_template_part( 'partials/header/info-blocks' ); ?>
			</div>
		</div>

	</header>

	<?php get_template_part( 'partials/header/page-header' ); ?>

	<div id="content" class="site-content" tabindex="-1">
