<?php
/**
 * The template for displaying 404 pages.
 *
 * @package GoodTailor
 */

get_header();
?>

<div class="container">
	<div class="main-wrap">
		<main id="main" class="main-content">

			<section class="error-404 not-found">
				<p><?php esc_html_e( 'It looks like nothing was found at this location. Maybe try a search?', 'goodtailor' ); ?></p>
				<?php get_search_form(); ?>
			</section>

		</main>
	</div>
</div>

<?php
get_footer();
