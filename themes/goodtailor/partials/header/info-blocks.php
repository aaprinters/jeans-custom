<?php
/**
 * Template for quick information blocks displayed in the header.
 *
 * @package GoodTailor
 */

if ( ! is_customize_preview() &&
	! goodtailor_is_active_header_info_block( 1 ) &&
	! goodtailor_is_active_header_info_block( 2 ) &&
	! goodtailor_is_active_header_info_block( 3 ) ) {
	return;
}
?>

<div class="site-header-blocks">

	<div class="site-header-blocks-column">
		<?php goodtailor_header_info_block( 1 ); ?>
	</div>

	<div class="site-header-blocks-column">
		<?php
		goodtailor_header_info_block( 2 );
		goodtailor_header_info_block( 3 );
		?>
	</div>

</div>
