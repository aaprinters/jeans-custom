<?php
/**
 * Displays header image, page title and breadcrumbs.
 *
 * @package GoodTailor
 */

if ( ! goodtailor_show_page_header() ) {
	return;
}
?>

<div class="page-header">

	<?php if ( has_header_image() ) : ?>
		<div class="page-header-image"><?php the_header_image_tag(); ?></div>
	<?php endif; ?>

	<div class="page-header-title-wrap">
		<div class="container">

			<?php if ( is_front_page() ) : ?>
				<h2 class="page-header-title"><?php goodtailor_the_page_title(); ?></h2>
			<?php else : ?>
				<h1 class="page-header-title"><?php goodtailor_the_page_title(); ?></h1>
				<?php goodtailor_breadcrumbs(); ?>
			<?php endif; ?>

		</div>
	</div>

	<div class="page-header-decor-top"></div>
	<div class="page-header-decor-bottom"></div>

</div>
