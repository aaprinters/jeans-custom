<?php
/**
 * Displays primary navigation menu.
 *
 * @package GoodTailor
 */

if ( ! has_nav_menu( 'primary' ) ) {
	return;
}
?>

<a class="site-header-menu-toggle" href="#site-navigation">
	<span class="screen-reader-text"><?php echo esc_html__( 'Toggle Primary menu', 'goodtailor' ); ?></span>
	<span class="menu-toggle-bar menu-toggle-bar-1"></span>
	<span class="menu-toggle-bar menu-toggle-bar-2"></span>
	<span class="menu-toggle-bar menu-toggle-bar-3"></span>
</a>

<nav id="site-navigation" class="site-header-menu primary-navigation" aria-label="<?php esc_attr_e( 'Primary menu', 'goodtailor' ); ?>">
	<?php wp_nav_menu( array( 'theme_location' => 'primary' ) ); ?>
</nav>
