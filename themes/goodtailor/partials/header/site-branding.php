<?php
/**
 * Displays custom logo.
 * Also displays site title and description for screen reader devices.
 *
 * @package GoodTailor
 */

?>

<div class="site-header-branding" itemscope itemtype="http://schema.org/Brand">

	<?php the_custom_logo(); ?>

	<?php goodtailor_site_branding_text(); ?>

</div>
