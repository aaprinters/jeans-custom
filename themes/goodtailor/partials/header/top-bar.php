<?php
/**
 * Template of the top bar strip above the header.
 *
 * @package GoodTailor
 */

if ( ! has_nav_menu( 'secondary' ) && ! has_nav_menu( 'top-bar-social' ) ) {
	return;
}
?>

<div class="site-header-top">
	<div class="container">

		<?php if ( has_nav_menu( 'secondary' ) ) : ?>
			<nav class="secondary-navigation">
				<?php wp_nav_menu( array( 'theme_location' => 'secondary' ) ); ?>
			</nav>
		<?php endif; ?>

		<?php if ( has_nav_menu( 'top-bar-social' ) ) : ?>
			<nav class="social-navigation">
				<?php
				wp_nav_menu( array(
					'theme_location' => 'top-bar-social',
					'depth'          => 2,
				) );
				?>
			</nav>
		<?php endif; ?>

	</div>
</div>
