<?php
/**
 * Template part for displaying blog post content.
 *
 * @package GoodTailor
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class( 'entry' ); ?>>

	<h1 class="entry-title"><?php the_title(); ?></h1>

	<?php goodtailor_post_meta(); ?>

	<?php if ( has_post_thumbnail() ) : ?>
		<div class="entry-featured-image"><?php the_post_thumbnail( 'goodtailor-featured-image' ); ?></div>
	<?php endif; ?>

	<div class="entry-content">
		<?php
		the_content();
		wp_link_pages( array(
			'before'      => '<div class="page-links">' . esc_html__( 'Pages:', 'goodtailor' ),
			'after'       => '</div>',
			'link_before' => '<span class="page-number">',
			'link_after'  => '</span>',
		) );
		?>
	</div>

	<footer class="entry-footer">

		<?php
		/* translators: used between list items, there is a space after the comma */
		$tags = get_the_tag_list( '', __( ', ', 'goodtailor' ) );
		if ( $tags ) :
			?>
			<div class="entry-tags">
				<?php
				/* translators: %s is a list of tags */
				echo wp_kses_post( sprintf( __( 'Tags: %s', 'goodtailor' ), $tags ) );
				?>
			</div>
		<?php endif; ?>

	</footer>

</article>
