<?php
/**
 * Template part for displaying post previews.
 *
 * @package GoodTailor
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class( 'post-entry' ); ?>>

	<?php if ( has_post_thumbnail() ) : ?>
		<a href="<?php the_permalink(); ?>">
			<div class="entry-featured-image"><?php the_post_thumbnail( 'goodtailor-featured-image' ); ?></div>
		</a>
	<?php endif; ?>

	<h2 class="entry-title"><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></h2>

	<div class="entry-excerpt"><?php the_excerpt(); ?></div>

	<?php goodtailor_post_meta(); ?>

</article>
