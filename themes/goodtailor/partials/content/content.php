<?php
/**
 * Template part for displaying content of singluar pages.
 *
 * @package GoodTailor
 */

?>

<div class="page-content-wrap">

	<?php
	the_content();
	wp_link_pages( array(
		'before' => '<div class="page-links">' . esc_html__( 'Pages: ', 'goodtailor' ),
		'after'  => '</div>',
	) );
	?>

</div>
