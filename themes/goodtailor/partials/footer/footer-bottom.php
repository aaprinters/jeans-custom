<?php
/**
 * Template for bottom part of the footer with footer tagline.
 *
 * @package GoodTailor
 */

?>

<div class="footer-bottom-wrap">
	<div class="container">

		<?php
		$footer_tagline = goodtailor_get_footer_tagline();
		if ( $footer_tagline || is_customize_preview() ) :
			?>
			<div class="footer-tagline">
				<?php echo wp_kses_post( $footer_tagline ); ?>
			</div>
		<?php endif; ?>

		<?php if ( has_nav_menu( 'footer-bottom' ) ) : ?>
			<nav class="footer-navigation">
				<?php
				wp_nav_menu( array(
					'theme_location' => 'footer-bottom',
					'depth'          => 1,
				) );
				?>
			</nav>
		<?php endif; ?>

	</div>
</div>
