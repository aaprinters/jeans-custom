# Copyright (C) 2020 BrothersTheme
# This file is distributed under the GNU General Public License v2 or later & Envato Market License.
msgid ""
msgstr ""
"Project-Id-Version: GoodTailor 1.5.2\n"
"Report-Msgid-Bugs-To: https://wordpress.org/support/theme/goodtailor\n"
"POT-Creation-Date: 2020-04-21 19:47:33+00:00\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"PO-Revision-Date: 2020-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"

#: 404.php:16
msgid "It looks like nothing was found at this location. Maybe try a search?"
msgstr ""

#. translators: %s: number of comments
#: comments.php:25
msgctxt "comments title"
msgid "%s Comment"
msgid_plural "%s Comments"
msgstr[0] ""
msgstr[1] ""

#: comments.php:45
msgid "Reply"
msgstr ""

#. translators: %s is an arrow left symbol
#: comments.php:54
msgid "%s Older Comments"
msgstr ""

#. translators: %s is an arrow right symbol
#: comments.php:60
msgid "Newer comments %s"
msgstr ""

#: comments.php:72
msgid "Comments are closed."
msgstr ""

#: functions.php:69
msgid "Primary Menu"
msgstr ""

#: functions.php:70
msgid "Secondary Menu"
msgstr ""

#: functions.php:71
msgid "Top Bar Social Menu"
msgstr ""

#: functions.php:72
msgid "Footer Bottom Menu"
msgstr ""

#: functions.php:87
msgid "Sidebar"
msgstr ""

#: functions.php:88
msgid "Add widgets here to appear in your sidebar."
msgstr ""

#: functions.php:96
msgid "Shop Sidebar"
msgstr ""

#: functions.php:97
msgid "Add widgets here to appear in your shop sidebar."
msgstr ""

#: functions.php:105
msgid "Footer 1"
msgstr ""

#: functions.php:106 functions.php:115 functions.php:124 functions.php:133
msgid "Add widgets here to appear in your footer."
msgstr ""

#: functions.php:114
msgid "Footer 2"
msgstr ""

#: functions.php:123
msgid "Footer 3"
msgstr ""

#: functions.php:132
msgid "Footer 4"
msgstr ""

#: functions.php:177
msgid "Expand submenu"
msgstr ""

#: functions.php:178
msgid "Collapse submenu"
msgstr ""

#: header.php:27
msgid "Skip to navigation"
msgstr ""

#: header.php:28
msgid "Skip to content"
msgstr ""

#. translators: 1: is current PHP version, 2: is current WordPress version
#: inc/back-compat.php:27
msgid ""
"GoodTailor Theme requires at least PHP 5.3 and WordPress 4.5. You are "
"running PHP %1$s and WordPress %2$s. Please upgrade and try again."
msgstr ""

#: inc/compat/easy-google-fonts.php:27
msgid "Body"
msgstr ""

#: inc/compat/easy-google-fonts.php:32
msgid "Headings"
msgstr ""

#: inc/compat/one-click-demo-import.php:31
msgid "GoodTailor Demo Content"
msgstr ""

#: inc/custom-fields.php:12
msgid "Page Settings"
msgstr ""

#: inc/custom-fields.php:16
msgid "Override Header Layout?"
msgstr ""

#: inc/custom-fields.php:18 inc/custom-fields.php:24
msgid "Don't Override"
msgstr ""

#: inc/custom-fields.php:19 inc/customize.php:136
msgid "Standard"
msgstr ""

#: inc/custom-fields.php:20 inc/customize.php:137
msgid "Transparent"
msgstr ""

#: inc/custom-fields.php:22
msgid "Override Header Color Style?"
msgstr ""

#: inc/custom-fields.php:25 inc/customize.php:152
msgid "Dark text on Light background"
msgstr ""

#: inc/custom-fields.php:26 inc/customize.php:153
msgid "Light text on Dark background"
msgstr ""

#: inc/custom-fields.php:28
msgid "Show Page Header?"
msgstr ""

#: inc/custom-fields.php:30 inc/custom-fields.php:36
msgid "Default"
msgstr ""

#: inc/custom-fields.php:31 inc/custom-fields.php:37 inc/customize.php:114
msgid "Show"
msgstr ""

#: inc/custom-fields.php:32 inc/custom-fields.php:38 inc/customize.php:115
msgid "Hide"
msgstr ""

#: inc/custom-fields.php:34
msgid "Show Breadcrumbs?"
msgstr ""

#: inc/customize.php:37
msgid "Light logo (for dark backgrounds)"
msgstr ""

#: inc/customize.php:82
msgid "Primary color"
msgstr ""

#: inc/customize.php:87
msgid "Header"
msgstr ""

#: inc/customize.php:96
msgid "Header background color"
msgstr ""

#: inc/customize.php:97
msgid ""
"Overlaid on top of Header image. If you don't see header image, you may need "
"to decrease the alpha value."
msgstr ""

#: inc/customize.php:112
msgid "Show breadcrumbs?"
msgstr ""

#: inc/customize.php:121
msgid "Header Layout"
msgstr ""

#: inc/customize.php:134
msgid "Global Header Layout"
msgstr ""

#: inc/customize.php:150
msgid "Header Color Style"
msgstr ""

#: inc/customize.php:159
msgid "Footer"
msgstr ""

#: inc/customize.php:167
msgid "Footer background image"
msgstr ""

#: inc/customize.php:183
msgid "Footer background color"
msgstr ""

#: inc/customize.php:184
msgid ""
"Overlaid on top of footer background image. If you don't see footer "
"background image, you may need to decrease the alpha value."
msgstr ""

#: inc/customize.php:195
msgid "Footer bottom background color"
msgstr ""

#: inc/customize.php:208
msgid "Footer tagline"
msgstr ""

#: inc/customize.php:219
msgid "Header Info Blocks"
msgstr ""

#: inc/customize.php:220
msgid ""
"Edit the quick information blocks shown in the header of the site (e.g. "
"contact information)"
msgstr ""

#. translators: %d is an index
#: inc/customize.php:235
msgid "Info Block %d"
msgstr ""

#: inc/customize.php:247
msgid "Text"
msgstr ""

#. translators: 1: is a link to the icon font website, 2: is the name of the
#. icon font, 3: is an example code snippet
#: inc/customize.php:264
msgid "<a href=\"%1$s\" target=\"_blank\">%2$s</a>, e.g. <code>%3$s</code>"
msgstr ""

#: inc/customize.php:273
msgid "Icon Font CSS class"
msgstr ""

#. translators: %s is a list of available icon fonts with usage examples
#: inc/customize.php:275
msgid "Enter a valid icon font CSS class.<br> Available icon fonts:<br> %1$s"
msgstr ""

#: inc/customize.php:289
msgid "Icon style"
msgstr ""

#: inc/customize.php:291
msgid "Normal"
msgstr ""

#: inc/customize.php:292
msgid "In circle"
msgstr ""

#: inc/customizer/class-rgba-color-control.php:68
msgid "Hex Value"
msgstr ""

#. translators: 1: is a link to the author portfolio page, 2: is the author
#. name
#: inc/defaults.php:26
msgid "&copy; 2017 GoodTailor WordPress Theme by <a href=\"%1$s\">%2$s</a>"
msgstr ""

#: inc/shop.php:60 inc/template-tags.php:129
msgid "Previous page"
msgstr ""

#: inc/shop.php:61 inc/template-tags.php:130
msgid "Next page"
msgstr ""

#. translators: If there are characters in your language that are not supported
#. by Vidaloka, translate this to 'off'. Do not translate into your own
#. language.
#: inc/template-functions.php:167
msgctxt "Vidaloka font: on or off"
msgid "on"
msgstr ""

#. translators: If there are characters in your language that are not supported
#. by Open Sans, translate this to 'off'. Do not translate into your own
#. language.
#: inc/template-functions.php:172
msgctxt "Open Sans font: on or off"
msgid "on"
msgstr ""

#: inc/template-functions.php:196 inc/template-functions.php:197
msgid "Name"
msgstr ""

#: inc/template-functions.php:198 inc/template-functions.php:199
msgid "Email"
msgstr ""

#: inc/template-functions.php:200 inc/template-functions.php:201
msgid "Website"
msgstr ""

#: inc/template-functions.php:202
msgctxt "noun"
msgid "Comment"
msgstr ""

#: inc/template-functions.php:329
msgctxt "relative time"
msgid "just now"
msgstr ""

#. translators: relative time format, e.g 10 minutes ago
#: inc/template-functions.php:332
msgctxt "relative time"
msgid "%s ago"
msgstr ""

#: inc/template-tags.php:47
msgid "Oops! That page can&rsquo;t be found."
msgstr ""

#. translators: Search results page title
#: inc/template-tags.php:54
msgid "Search"
msgstr ""

#: inc/template-tags.php:56
msgid "Nothing found"
msgstr ""

#: inc/template-tags.php:111
msgid "View your shopping cart"
msgstr ""

#: inc/template-tags.php:131
msgid "Page"
msgstr ""

#: inc/template-tags.php:146
msgid "Featured post"
msgstr ""

#. translators: %s is the post author
#: inc/template-tags.php:158
msgid "Author: %s"
msgstr ""

#. translators: used between list items, there is a space after the comma
#: inc/template-tags.php:164 partials/content/content-post.php:36
msgid ", "
msgstr ""

#. translators: %s is a list of categories
#: inc/template-tags.php:173
msgid "Category: %s"
msgstr ""

#. translators: %d is the number of comments
#: inc/template-tags.php:190
msgid "%d comment"
msgid_plural "%d comments"
msgstr[0] ""
msgstr[1] ""

#: inc/template-tags.php:222
msgctxt "comment author label"
msgid "Author"
msgstr ""

#: inc/template-tags.php:237
msgid "Edit"
msgstr ""

#: inc/template-tags.php:247
msgid "Your comment is awaiting moderation."
msgstr ""

#: inc/tgmpa/tgm-plugin-activation.php:16
msgid "GoodTailor Custom Post Types & Shortcodes Plugin"
msgstr ""

#: inc/tgmpa/tgm-plugin-activation.php:23
msgid "Breadcrumb NavXT"
msgstr ""

#: inc/tgmpa/tgm-plugin-activation.php:28
msgid "Carbon Fields"
msgstr ""

#: inc/tgmpa/tgm-plugin-activation.php:35
msgid "Contact Form 7"
msgstr ""

#: inc/tgmpa/tgm-plugin-activation.php:40
msgid "Easy Google Fonts"
msgstr ""

#: inc/tgmpa/tgm-plugin-activation.php:45
msgid "WPBakery Visual Composer"
msgstr ""

#: inc/tgmpa/tgm-plugin-activation.php:52
msgid "MailChimp for WordPress"
msgstr ""

#: inc/tgmpa/tgm-plugin-activation.php:57
msgid "One Click Demo Import"
msgstr ""

#: inc/tgmpa/tgm-plugin-activation.php:62
msgid "Slider Revolution"
msgstr ""

#: inc/tgmpa/tgm-plugin-activation.php:69
msgid "WooCommerce"
msgstr ""

#. translators: %s is a link to create new post
#: partials/content/content-none.php:15
msgid ""
"Ready to publish your first post? <a href=\"%1$s\">Get started here</a>."
msgstr ""

#: partials/content/content-none.php:19
msgid ""
"Sorry, but nothing matched your search terms. Please try again with some "
"different keywords."
msgstr ""

#: partials/content/content-none.php:21
msgid ""
"It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps "
"searching can help."
msgstr ""

#: partials/content/content-post.php:24
msgid "Pages:"
msgstr ""

#. translators: %s is a list of tags
#: partials/content/content-post.php:42
msgid "Tags: %s"
msgstr ""

#: partials/content/content.php:15
msgid "Pages: "
msgstr ""

#: partials/header/primary-navigation.php:14
msgid "Toggle Primary menu"
msgstr ""

#: partials/header/primary-navigation.php:20
msgid "Primary menu"
msgstr ""

#: searchform.php:12 woocommerce/product-searchform.php:11
msgctxt "label"
msgid "Search for:"
msgstr ""

#: searchform.php:13
msgctxt "placeholder"
msgid "Search &hellip;"
msgstr ""

#: searchform.php:14 woocommerce/product-searchform.php:13
msgctxt "submit button"
msgid "Search"
msgstr ""

#: woocommerce/product-searchform.php:12
msgctxt "placeholder"
msgid "Search products&hellip;"
msgstr ""

#. Theme Name of the plugin/theme
msgid "GoodTailor"
msgstr ""

#. Theme URI of the plugin/theme
msgid "https://demo.brothersthemes.com/goodtailor/"
msgstr ""

#. Description of the plugin/theme
msgid "Premium WordPress theme."
msgstr ""

#. Author of the plugin/theme
msgid "BrothersTheme"
msgstr ""

#. Author URI of the plugin/theme
msgid "https://themeforest.net/user/brotherstheme"
msgstr ""
