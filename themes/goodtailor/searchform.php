<?php
/**
 * The template for displaying search form
 *
 * @package GoodTailor
 */

$unique_id = esc_attr( uniqid( 'search-form-' ) );
?>

<form role="search" method="get" class="search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
	<label class="screen-reader-text" for="<?php echo esc_attr( $unique_id ); ?>"><?php echo esc_html_x( 'Search for:', 'label', 'goodtailor' ); ?></label>
	<input type="search" id="<?php echo esc_attr( $unique_id ); ?>" class="search-field" placeholder="<?php echo esc_attr_x( 'Search &hellip;', 'placeholder', 'goodtailor' ); ?>" value="<?php echo get_search_query(); ?>" name="s" />
	<button type="submit" class="search-submit"><span class="icon typcn typcn-zoom-outline"></span><span class="screen-reader-text"><?php echo esc_html_x( 'Search', 'submit button', 'goodtailor' ); ?></span></button>
</form>
