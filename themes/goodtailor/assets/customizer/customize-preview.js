/**
 * Instantly live-update customizer settings in the preview for improved user experience.
 */

( function( $ ) {
	'use strict';

	// Site Branding Text visibility customization
	wp.customize( 'header_textcolor', function( value ) {
		value.bind( function( to ) {
			if ( 'blank' === to ) {
				$( 'body' ).addClass( 'hide-site-branding-text' );
			} else {
				$( 'body' ).removeClass( 'hide-site-branding-text' );
			}
		} );
	} );

	// Breadcrumbs customization
	wp.customize( 'show_breadcrumbs', function( value ) {
		if ( 'hide' === value.get() ) {
			$( '.breadcrumbs' ).css( 'display', 'none' );
		}
		value.bind( function( to ) {
			if ( 'hide' === to ) {
				$( '.breadcrumbs' ).css( 'display', 'none' );
			} else {
				$( '.breadcrumbs' ).css( 'display', '' );
			}
		} );
	} );

	// Header background color customization
	wp.customize( 'header_bg_color', function( value ) {
		value.bind( function( to ) {
			$( '.page-header-title-wrap' ).css( 'background-color', to );
		} );
	} );

	// Footer background color customization
	wp.customize( 'footer_bg_color', function( value ) {
		value.bind( function( to ) {
			$( '.footer-top-wrap' ).css( 'background-color', to );
		} );
	} );

	// Footer bottom background color customization
	wp.customize( 'footer_bottom_bg_color', function( value ) {
		value.bind( function( to ) {
			$( '.footer-bottom-wrap' ).css( 'background-color', to );
		} );
	} );

	// Custom colors
	$.each( [ 'primary_color', 'primary_color_light' ], function( index, id ) {
		wp.customize( id, function( setting ) {
			setting.bind( function( to ) {
				var $style       = $( '#goodtailor-custom-' + id );
				var css          = $style.html();
				var currentColor = $style.data( 'current-color' );

				css = css.split( ' ' + currentColor ).join( ' ' + to );
				$style.html( css ).data( 'current-color', to );
			} );
		} );
	} );

	// Primary Button colors
	function updatePrimaryButtonColors() {
		var $style        = $( '#goodtailor-custom-primary-button-colors' );
		var css           = $style.html();
		var currentColor1 = $style.data( 'current-color-1' );
		var currentColor2 = $style.data( 'current-color-2' );
		var newColor1     = wp.customize( 'primary_button_color_1' ).get();
		var newColor2     = wp.customize( 'primary_button_color_2' ).get();

		if ( currentColor1 !== newColor1 ) {
			css = css.split( currentColor1 + ' /*1*/' ).join( newColor1 + ' /*1*/' );
			$style.data( 'current-color-1', newColor1 );
		}
		if ( currentColor2 !== newColor2 ) {
			css = css.split( currentColor2 + ' /*2*/' ).join( newColor2 + ' /*2*/' );
			$style.data( 'current-color-2', newColor2 );
		}
		if ( $style.html() !== css ) {
			$style.html( css );
		}
	}

	wp.customize( 'primary_button_color_1', function( setting ) {
		setting.bind( updatePrimaryButtonColors );
	} );
	wp.customize( 'primary_button_color_2', function( setting ) {
		setting.bind( updatePrimaryButtonColors );
	} );

	// Header Info Blocks
	$.each( [ 1, 2, 3 ], function( index, id ) {
		var iconSel = '.js-header-info-block-' + id + ' .header-info-block-icon';

		wp.customize( 'header_info_block_icon_class_' + id, function( value ) {
			value.bind( function( to ) {
				$( iconSel ).html( '<span class="' + to + '"></span>' );
			} );
		} );

		wp.customize( 'header_info_block_icon_style_' + id, function( value ) {
			value.bind( function( to, from ) {
				$( iconSel )
					.removeClass( 'header-info-block-icon--' + from )
					.addClass( 'header-info-block-icon--' + to );
			} );
		} );
	} );

}( jQuery ) );
