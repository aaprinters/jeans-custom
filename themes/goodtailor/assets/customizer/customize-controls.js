/* eslint curly:off */
( function( $ ) {
	'use strict';

	function hexToRgb( hex ) {
		var r, g, b;
		if ( ! hex ) {
			return null;
		}
		if ( hex[0] === '#' ) {
			hex = hex.substring( 1 );
		}
		r = parseInt( hex.substring( 0, 2 ), 16 );
		g = parseInt( hex.substring( 2, 4 ), 16 );
		b = parseInt( hex.substring( 4, 6 ), 16 );
		return { r: r, g: g, b: b };
	}

	function rgbToHex( rgb ) {
		if ( typeof rgb !== 'object' ) {
			return null;
		}
		return '#' +
			( '0' + Math.round( rgb.r ).toString( 16 ) ).slice( -2 ) +
			( '0' + Math.round( rgb.g ).toString( 16 ) ).slice( -2 ) +
			( '0' + Math.round( rgb.b ).toString( 16 ) ).slice( -2 );
	}

	// Copied from libsass
	function rgbToHsl( rgb ) {
		var r, g, b, max, min, delta, h, s, l;
		if ( typeof rgb !== 'object' ) {
			return null;
		}
		// Algorithm from http://en.wikipedia.org/wiki/wHSL_and_HSV#Conversion_from_rgbToHsl_or_HSV
		r = rgb.r / 255;
		g = rgb.g / 255;
		b = rgb.b / 255;
		max = Math.max( r, Math.max( g, b ) );
		min = Math.min( r, Math.min( g, b ) );
		delta = max - min;
		h = 0;
		l = ( max + min ) / 2;
		if ( max === min ) {
			h = s = 0; // achromatic
		} else {
			if ( l < 0.5 ) {
				s = delta / ( max + min );
			} else {
				s = delta / ( 2 - max - min );
			}

			if ( r === max ) {
				h = ( g - b ) / delta + ( g < b ? 6 : 0 );
			} else if ( g === max ) {
				h = ( b - r ) / delta + 2;
			} else if ( b === max ) {
				h = ( r - g ) / delta + 4;
			}
		}
		h = h / 6 * 360;
		s = s * 100;
		l = l * 100;
		return { h: h, s: s, l: l };
	}

	// Copied from libsass
	function hslToRgb( hsl ) {
		var h, s, l, m1, m2, r, g, b;
		if ( typeof hsl !== 'object' ) {
			return;
		}

		function hToRgb( m1, m2, h ) {
			while ( h < 0 ) h += 1;
			while ( h > 1 ) h -= 1;
			if ( h * 6 < 1 ) return m1 + ( m2 - m1 ) * h * 6;
			if ( h * 2 < 1 ) return m2;
			if ( h * 3 < 2 ) return m1 + ( m2 - m1 ) * ( 2 / 3 - h ) * 6;
			return m1;
		}

		h = hsl.h / 360;
		s = hsl.s / 100;
		l = hsl.l / 100;
		if ( l < 0 ) l = 0;
		if ( s < 0 ) s = 0;
		if ( l > 1 ) l = 1;
		if ( s > 1 ) s = 1;
		while ( h < 0 ) h += 1;
		while ( h > 0 ) h -= 1;

		// if saturation is exacly zero, we loose
		// information for hue, since it will evaluate
		// to zero if converted back from rgb. Setting
		// saturation to a very tiny number solves this.
		if ( s == 0 ) s = 1e-10;

		// Algorithm from the CSS3 spec: http://www.w3.org/TR/css3-color/#hsl-color.
		if ( l <= 0.5 ) {
			m2 = l * ( s + 1 );
		} else {
			m2 = ( l + s ) - ( l * s );
		}
		m1 = l * 2 - m2;
		r = hToRgb( m1, m2, h + 1 / 3 ) * 255;
		g = hToRgb( m1, m2, h ) * 255;
		b = hToRgb( m1, m2, h - 1 / 3 ) * 255;
		return { r: r, g: g, b: b };
	}

	// Copied from libsass
	function lighten( color, amount ) {
		var rgbColor = hexToRgb( color );
		var hslColor = rgbToHsl( rgbColor );
		if ( typeof hslColor !== 'object' ) {
			return null;
		}
		// Check lightness is not negative before lighten it
		if ( hslColor.l < 0 ) {
			hslColor.l = 0;
		}
		hslColor.l += amount;
		return rgbToHex( hslToRgb( hslColor ) );
	}

	// Copied from libsass
	function darken( color, amount ) {
		var rgbColor = hexToRgb( color );
		var hslColor = rgbToHsl( rgbColor );
		if ( typeof hslColor !== 'object' ) {
			return null;
		}
		// Check lightness is not over 100, before darken it
		if ( hslColor.l > 100 ) {
			hslColor.l = 100;
		}
		hslColor.l -= amount;
		return rgbToHex( hslToRgb( hslColor ) );
	}

	wp.customize.bind( 'ready', function() {
		wp.customize( 'primary_color', function( setting ) {
			setting.bind( function( to ) {
				wp.customize( 'primary_color_light' ).set( lighten( to, 10 ) );
				wp.customize( 'primary_button_color_1' ).set( lighten( to, 5 ) );
				wp.customize( 'primary_button_color_2' ).set( darken( to, 8 ) );
			} );
		} );
	} );

} ( jQuery ) );
