( function( $ ) {
	'use strict';

	var isRtl = $( 'body' ).hasClass( 'rtl' );

	// Set up mobile menu toggle button.
	( function() {
		var $menu       = $( '.site-header-menu' );
		var $menuLink   = $( 'a.site-header-menu-toggle' );
		var $menuButton = $( '<button />', { 'class': 'site-header-menu-toggle', 'aria-expanded': false } )
			.append( $menuLink.children() );

		$menuLink.replaceWith( $menuButton );

		$menuButton.on( 'click.goodtailor', function() {
			$menu.toggleClass( 'is-open' );
			$( this ).toggleClass( 'is-open' ).attr( 'aria-expanded', $menu.hasClass( 'is-open' ) );
			$( 'html, body' ).toggleClass( 'is-menu-open' );
		} );
	}() );

	// Set up mobile menu submenu toggling.
	( function() {
		var $container    = $( '.primary-navigation' );
		var $toggleButton = $( '<button />', { 'class': 'sub-menu-toggle', 'aria-expanded': false } )
			.append( $( '<span />', { 'class': 'sub-menu-toggle-icon' } ) )
			.append( $( '<span />', { 'class': 'screen-reader-text', 'text': goodtailorScreenReaderText.expand } ) );

		$container.find( '.menu-item-has-children > a, .page_item_has_children > a' )
			.filter( function() {
				return $( this ).closest( '.children, .sub-menu' ).length == 0;
			} )
			.after( $toggleButton );

		$container.find( '.current-menu-ancestor > .sub-menu-toggle' )
			.addClass( 'is-open' )
			.attr( 'aria-expanded', 'true' )
			.find( '.screen-reader-text' )
			.text( goodtailorScreenReaderText.collapse );
		$container.find( '.current-menu-ancestor > .children, .current-menu-ancestor > .sub-menu' )
			.filter( function() {
				return $( this ).parent().closest( '.children, .sub-menu' ).length == 0;
			} )
			.addClass( 'is-open' );

		$container.find( '.sub-menu-toggle' ).on( 'click.goodtailor', function( event ) {
			var $this = $( this );

			event.preventDefault();
			$this.toggleClass( 'is-open' );
			$this.next( '.children, .sub-menu' ).toggleClass( 'is-open' );
			$this.attr( 'aria-expanded', $this.hasClass( 'is-open' ) );
			$this.find( '.screen-reader-text' )
				.text( $this.hasClass( 'is-open' ) ? goodtailorScreenReaderText.collapse : goodtailorScreenReaderText.expand );
		} );
	}() );

	// Add has-focus class to menu items parents
	$( '.primary-navigation, .secondary-navigation' ).on( 'focus.goodtailor blur.goodtailor', 'a', function( event ) {
		if ( event.type === 'focusin' ) {
			$( this ).parents().addClass( 'has-focus' );
		} else {
			$( this ).parents().removeClass( 'has-focus' );
		}
	} );

	// Smooth scroll to top
	$( 'a[href="#top"]' ).on( 'click.goodtailor', function() {
		$( 'html, body' ).animate( { scrollTop: 0 }, 1000 );
		return false;
	} );

	function clampValue( from, to, value ) {
		return Math.max( from, Math.min( to, value ) );
	}

	function owlMakeItemHeightsEqual( carousel ) {
		var $carousel = $( carousel );
		var maxHeight = 0;
		$carousel.find( '.owl-item:not(.cloned)' ).height( 'auto' ).each( function() {
			maxHeight = Math.max( maxHeight, $( this ).height() );
		} );
		$carousel.find( '.owl-item' ).height( maxHeight );
	}

	// Carousel setup
	if ( $( '.js-bt-carousel' ).length ) {
		$( '.js-bt-carousel' ).each( function( index, element ) {
			var $element = $( element );
			var items    = $element.data( 'items' ) !== undefined ? $element.data( 'items' ) : 1;
			var config   = {
				items: 1,
				center: false,
				margin: 30,
				rtl: isRtl,
				loop: true,
				nav: false,
				dots: true,
				autoplay: false,
				responsive: {
					768: {
						items: clampValue( 1, 2, items - 1 ),
					},
					970: {
						items: clampValue( 1, 3, items ),
					},
					1200: {
						items: clampValue( 1, 4, items ),
					},
				},
			};

			if ( $element.data( 'equal-heights' ) !== undefined ) {
				config.onInitialized = function( event ) {
					owlMakeItemHeightsEqual( event.target );
					setTimeout( function() {
						owlMakeItemHeightsEqual( event.target );
					}, 100 );
				};
				config.onResized = function( event ) {
					owlMakeItemHeightsEqual( event.target );
				};
				$element.imagesLoaded().progress( function( instance, image ) {
					owlMakeItemHeightsEqual( instance.elements[0] );
				} );
			}

			$element.owlCarousel( config );
		} );
	}

	function shuffleDisableResize( resize, wait ) {
		return function() {};
	};

	function shuffleDelayAndThrottle( update, delayAmount ) {
		var timeoutID;

		function callUpdate() {
			timeoutID = 0;
			update();
		}

		return function delayed() {
			if ( timeoutID ) {
				clearTimeout( timeoutID );
			}
			timeoutID = setTimeout( callUpdate, delayAmount );
		};
	};

	// Grid setup
	if ( $( '.js-bt-grid' ).length ) {
		$( '.js-bt-grid' ).each( function( index, element ) {
			var $element       = $( element );
			var itemsContainer = $element.find( '.bt-grid-items-container' )[0];
			var $filters       = $element.find( '.bt-grid-filters-container' );
			var $activeFilter  = $filters.find( '.is-active' );
			var shuffle        = new window.Shuffle( itemsContainer, {
				itemSelector: '.bt-grid-item',
				throttle: shuffleDisableResize,
			} );
			var update, throttledUpdate;

			// Handle filtering.
			$filters.on( 'click.goodtailor', '.bt-grid-filter', function( event ) {
				var $target = $( event.target );

				event.target.blur();
				if ( ! $target.hasClass( 'is-active' ) ) {
					$target.addClass( 'is-active' );
					$activeFilter.removeClass( 'is-active' );
					$activeFilter = $target;
					shuffle.filter( $target.data( 'group' ) );
				}
			} );

			// Handle updates on window resize.
			if ( $element.data( 'layout-method' ) === 'masonry' ) {
				// Masonry layout
				update = function() {
					shuffle.update();
				};
			} else {
				// Grid layout
				update = function() {
					// Make all items equal height.
					var $items    = $element.find( '.bt-grid-item' );
					var maxHeight = 0;

					$items.height( 'auto' ).each( function() {
						maxHeight = Math.max( maxHeight, $( this ).height() );
					} );
					$items.height( maxHeight );
					shuffle.update();
				};
			}

			throttledUpdate = shuffleDelayAndThrottle( update, 100 );

			imagesLoaded( itemsContainer ).on( 'progress', update );
			throttledUpdate();
			$( window ).resize( throttledUpdate );
		} );
	}

}( jQuery ) );
