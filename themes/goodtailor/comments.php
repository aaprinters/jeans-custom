<?php
/**
 * The template for displaying comments
 *
 * This is the template that displays the area of the page that contains both the current comments
 * and the comment form.
 *
 * @package GoodTailor
 */

if ( post_password_required() ) {
	return;
}
?>

<div id="comments" class="comments-area">

	<?php if ( have_comments() ) : ?>

		<h2 class="comments-title">
			<?php
			$comments_number = get_comments_number();
			printf(
				/* translators: %s: number of comments */
				esc_html( _nx(
					'%s Comment',
					'%s Comments',
					$comments_number,
					'comments title',
					'goodtailor'
				) ),
				esc_html( number_format_i18n( $comments_number ) )
			);
			?>
		</h2>

		<ol class="comments-list">
			<?php
			wp_list_comments( array(
				'avatar_size' => 64,
				'style'       => 'ol',
				'short_ping'  => true,
				'max_depth'   => 2,
				'callback'    => 'goodtailor_render_comment',
				'reply_text'  => esc_html__( 'Reply', 'goodtailor' ),
			) );
			?>
		</ol>

		<?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : ?>
			<nav class="comments-nav">
				<?php
				/* translators: %s is an arrow left symbol */
				$prev_label = sprintf( esc_html__( '%s Older Comments', 'goodtailor' ), '<span class="typcn typcn-arrow-left-thick"></span>' );
				$prev_link = get_previous_comments_link( $prev_label );
				if ( $prev_link ) {
					echo '<div class="comments-nav-prev">' . wp_kses_post( $prev_link ) . '</div>';
				}
				/* translators: %s is an arrow right symbol */
				$next_label = sprintf( esc_html__( 'Newer comments %s', 'goodtailor' ), '<span class="typcn typcn-arrow-right-thick"></span>' );
				$next_link = get_next_comments_link( $next_label );
				if ( $next_link ) {
					echo '<div class="comments-nav-next">' . wp_kses_post( $next_link ) . '</div>';
				}
				?>
			</nav>
		<?php endif; ?>

	<?php endif; ?>

	<?php if ( ! comments_open() && get_comments_number() && post_type_supports( get_post_type(), 'comments' ) ) : ?>
		<p class="comments-closed"><?php echo esc_html__( 'Comments are closed.', 'goodtailor' ); ?></p>
	<?php endif; ?>

	<?php comment_form(); ?>

</div>
