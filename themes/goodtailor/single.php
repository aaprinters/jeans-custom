<?php
/**
 * The template for displaying single custom posts (e.g single Service)
 *
 * @package GoodTailor
 */

get_header();

do_action( 'goodtailor_single_main_before' );
?>

<main id="main" class="main-content">

	<?php
	while ( have_posts() ) : the_post();

		get_template_part( 'partials/content/content', get_post_type() );

		if ( comments_open() || get_comments_number() ) :
			comments_template();
		endif;

	endwhile;
	?>

</main>

<?php
do_action( 'goodtailor_single_main_after' );

get_footer();
