<?php
/**
 * Template for displaying single pages.
 *
 * @package GoodTailor
 */

get_header();

do_action( 'goodtailor_page_main_before' );
?>

<main id="main" class="main-content">

	<?php
	while ( have_posts() ) : the_post();

		get_template_part( 'partials/content/content', 'page' );

		if ( comments_open() || get_comments_number() ) :
			comments_template();
		endif;

	endwhile;
	?>

</main>

<?php
do_action( 'goodtailor_page_main_after' );

get_footer();
