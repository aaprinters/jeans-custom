<?php
/**
 * The footer of our theme.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @package GoodTailor
 */

?>

	</div> <!-- #content -->

	<footer id="colophon" class="site-footer">

		<?php
		get_template_part( 'partials/footer/footer-widgets' );
		get_template_part( 'partials/footer/footer-bottom' );
		?>

	</footer>

<?php wp_footer(); ?>
</body>
</html>
