<?php
/**
 * The sidebar containing the main widget area.
 *
 * @package GoodTailor
 */

if ( ! is_active_sidebar( 'sidebar-1' ) ) {
	return;
}
?>

<div class="sidebar-wrap">
	<aside id="secondary" class="sidebar-content">
		<?php dynamic_sidebar( 'sidebar-1' ); ?>
	</aside>
</div>
