<?php
/**
 * The template for displaying product search form
 *
 * @package GoodTailor
 */

?>

<form role="search" method="get" class="search-form woocommerce-product-search" action="<?php echo esc_url( home_url( '/' ) ); ?>">
	<label class="screen-reader-text" for="woocommerce-product-search-field-<?php echo isset( $index ) ? absint( $index ) : 0; ?>"><?php echo esc_html_x( 'Search for:', 'label', 'goodtailor' ); ?></label>
	<input type="search" id="woocommerce-product-search-field-<?php echo isset( $index ) ? absint( $index ) : 0; ?>" class="search-field" placeholder="<?php echo esc_attr_x( 'Search products&hellip;', 'placeholder', 'goodtailor' ); ?>" value="<?php echo get_search_query(); ?>" name="s" />
	<button type="submit" class="search-submit"><span class="icon typcn typcn-zoom-outline"></span><span class="screen-reader-text"><?php echo esc_html_x( 'Search', 'submit button', 'goodtailor' ); ?></span></button>
	<input type="hidden" name="post_type" value="product" />
</form>
