<?php
/**
 * Theme functions, settings and definitions.
 *
 * @package GoodTailor
 */

define( 'GOODTAILOR_THEME_VERSION', ( WP_DEBUG ) ? time() : wp_get_theme()->get( 'Version' ) );

/**
 * This theme requires PHP 5.3+ and WordPress 4.5+
 */
if ( version_compare( phpversion(), '5.3', '<' ) || version_compare( $GLOBALS['wp_version'], '4.5', '<' ) ) {
	require get_template_directory() . '/inc/back-compat.php';
	return;
}

/**
 * Set up the theme and register supports for various WordPress features.
 */
function goodtailor_setup() {
	// Theme translation support
	load_theme_textdomain( 'goodtailor', get_template_directory() . '/languages' );
	// Theme supports
	add_theme_support( 'automatic-feed-links' );
	add_theme_support( 'title-tag' );
	add_theme_support( 'post-thumbnails' );
	add_image_size( 'goodtailor-featured-image', 1600, 920, true );
	add_theme_support( 'custom-logo', array(
		'width'      => 250,
		'height'     => 250,
		'flex-width' => true,
	) );
	add_theme_support( 'custom-header', array(
		'uploads'     => true,
		'flex-width'  => true,
		'width'       => 2000,
		'flex-height' => true,
		'height'      => 600,
	) );
	add_theme_support( 'custom-background', array(
		'default-color' => 'ffffff',
	) );
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );
	add_theme_support( 'post-formats', array(
		'aside',
		'image',
		'video',
		'quote',
		'link',
		'gallery',
		'status',
	) );
	// Selective refresh for widgets
	add_theme_support( 'customize-selective-refresh-widgets' );
	// WooCommerce support.
	add_theme_support( 'woocommerce' );
	add_theme_support( 'wc-product-gallery-zoom' );
	add_theme_support( 'wc-product-gallery-lightbox' );
	add_theme_support( 'wc-product-gallery-slider' );
	// Navigation menus
	register_nav_menus( array(
		'primary'        => esc_html__( 'Primary Menu', 'goodtailor' ),
		'secondary'      => esc_html__( 'Secondary Menu', 'goodtailor' ),
		'top-bar-social' => esc_html__( 'Top Bar Social Menu', 'goodtailor' ),
		'footer-bottom'  => esc_html__( 'Footer Bottom Menu', 'goodtailor' ),
	) );
	// Set the default content width
	$GLOBALS['content_width'] = 820;
	// Add additional styles for the visual editor
	add_editor_style( 'assets/css/editor-style.css' );
}
add_action( 'after_setup_theme', 'goodtailor_setup' );

/**
 * Register theme sidebars and widgetized areas.
 */
function goodtailor_widgets_init() {
	register_sidebar( array(
		'id'            => 'sidebar-1',
		'name'          => esc_html__( 'Sidebar', 'goodtailor' ),
		'description'   => esc_html__( 'Add widgets here to appear in your sidebar.', 'goodtailor' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
	register_sidebar( array(
		'id'            => 'shop-sidebar-1',
		'name'          => esc_html__( 'Shop Sidebar', 'goodtailor' ),
		'description'   => esc_html__( 'Add widgets here to appear in your shop sidebar.', 'goodtailor' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
	register_sidebar( array(
		'id'            => 'footer-1',
		'name'          => esc_html__( 'Footer 1', 'goodtailor' ),
		'description'   => esc_html__( 'Add widgets here to appear in your footer.', 'goodtailor' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
	register_sidebar( array(
		'id'            => 'footer-2',
		'name'          => esc_html__( 'Footer 2', 'goodtailor' ),
		'description'   => esc_html__( 'Add widgets here to appear in your footer.', 'goodtailor' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
	register_sidebar( array(
		'id'            => 'footer-3',
		'name'          => esc_html__( 'Footer 3', 'goodtailor' ),
		'description'   => esc_html__( 'Add widgets here to appear in your footer.', 'goodtailor' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
	register_sidebar( array(
		'id'            => 'footer-4',
		'name'          => esc_html__( 'Footer 4', 'goodtailor' ),
		'description'   => esc_html__( 'Add widgets here to appear in your footer.', 'goodtailor' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'goodtailor_widgets_init' );

/**
 * Enqueue all the necessary scripts & styles.
 */
function goodtailor_enqueue_scripts() {
	// Google Fonts
	// phpcs:ignore WordPress.WP.EnqueuedResourceParameters.MissingVersion
	wp_enqueue_style( 'goodtailor-google-fonts', goodtailor_google_fonts_url() );
	// Icon fonts
	wp_enqueue_style( 'font-awesome', get_template_directory_uri() . '/assets/libs/font-awesome/css/font-awesome.min.css', array(), '4.7.0' );
	wp_enqueue_style( 'themify-icons', get_template_directory_uri() . '/assets/libs/themify-icons/themify-icons.css', array(), '1.0.1' );
	wp_enqueue_style( 'typicons', get_template_directory_uri() . '/assets/libs/typicons/typicons.min.css', array(), '2.0.9-b2' );
	// imagesLoaded
	wp_enqueue_script( 'imagesloaded', get_template_directory_uri() . '/assets/libs/imagesloaded/imagesloaded.pkgd.min.js', array( 'jquery' ), '4.1.4', true );
	// Modernizr (custom)
	wp_enqueue_script( 'modernizr-custom', get_template_directory_uri() . '/assets/libs/modernizr-custom/modernizr-custom.min.js', array(), GOODTAILOR_THEME_VERSION, false );
	// Owl Carousel
	wp_enqueue_style( 'owl-carousel', get_template_directory_uri() . '/assets/libs/owl.carousel/owl.carousel.min.css', array(), '2.2.1' );
	wp_enqueue_script( 'owl-carousel', get_template_directory_uri() . '/assets/libs/owl.carousel/owl.carousel.min.js', array( 'jquery' ), '2.2.1', true );
	// Shuffle
	wp_enqueue_script( 'shuffle', get_template_directory_uri() . '/assets/libs/shuffle/shuffle.min.js', array(), '5.2.3', true );
	// Theme stylesheet
	wp_enqueue_style( 'goodtailor-style', get_template_directory_uri() . '/assets/css/style.css', array(), GOODTAILOR_THEME_VERSION );
	// Theme custom scripts
	wp_enqueue_script( 'goodtailor-script', get_template_directory_uri() . '/assets/js/custom.js', array( 'jquery' ), GOODTAILOR_THEME_VERSION, true );
	// Comments script
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
	// Inline styles
	$inline_css = goodtailor_get_inline_style();
	if ( ! empty( $inline_css ) ) {
		wp_add_inline_style( 'goodtailor-style', $inline_css );
	}
	// Localization
	$goodtailor_l10n = array(
		'expand'   => __( 'Expand submenu', 'goodtailor' ),
		'collapse' => __( 'Collapse submenu', 'goodtailor' ),
	);
	wp_localize_script( 'goodtailor-script', 'goodtailorScreenReaderText', $goodtailor_l10n );
}
add_action( 'wp_enqueue_scripts', 'goodtailor_enqueue_scripts' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 */
function goodtailor_content_width() {
	$content_width = $GLOBALS['content_width'];
	if ( ! is_active_sidebar( 'sidebar-1' ) || ( is_singular() && 'post' !== get_post_type() ) ) {
		$content_width = 1140;
	}
	$GLOBALS['content_width'] = apply_filters( 'goodtailor_content_width', $content_width );
}
add_action( 'template_redirect', 'goodtailor_content_width', 0 );

/**
 * Theme default values
 */
require get_template_directory() . '/inc/defaults.php';

/**
 * Additional features to allow styling of the templates
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Custom template tags
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Customizer settings
 */
require get_template_directory() . '/inc/customize.php';

/**
 * Custom colors
 */
require get_template_directory() . '/inc/custom-colors.php';

/**
 * Custom fields
 */
require get_template_directory() . '/inc/custom-fields.php';

/**
 * Display recommended plugins in the admin dashboard
 */
require get_template_directory() . '/inc/tgmpa/tgm-plugin-activation.php';

/**
 * Compatibility with WooCommerce
 */
require get_template_directory() . '/inc/shop.php';

/**
 * Compatibility with other plugins
 */
require get_template_directory() . '/inc/compat/breadcrumb-navxt.php';
require get_template_directory() . '/inc/compat/easy-google-fonts.php';
require get_template_directory() . '/inc/compat/one-click-demo-import.php';
require get_template_directory() . '/inc/compat/revolution-slider.php';
require get_template_directory() . '/inc/compat/wpbakery-page-builder.php';
