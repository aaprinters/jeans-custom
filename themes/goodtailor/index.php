<?php
/**
 * The main template file.
 *
 * @package GoodTailor
 */

get_header();
?>

<div class="container">

	<div class="main-wrap">
		<main id="main" class="main-content">

			<?php if ( have_posts() ) : ?>

				<div class="archive-loop-wrap">
					<?php while ( have_posts() ) : the_post(); ?>

						<div class="archive-loop-item">
							<?php get_template_part( 'partials/content/content', 'preview' ); ?>
						</div>

					<?php endwhile; ?>
				</div>

				<?php goodtailor_pagination(); ?>

			<?php else : ?>

				get_template_part( 'partials/content/content', 'none' );

			<?php endif; ?>

		</main>
	</div>

	<?php get_sidebar(); ?>

</div>

<?php
get_footer();
