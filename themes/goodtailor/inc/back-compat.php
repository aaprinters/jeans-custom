<?php
/**
 * Back compat functionality
 *
 * @package GoodTailor
 */

/**
 * Prevent switching to this theme on old versions of PHP or WordPress.
 */
function goodtailor_switch_theme( $old_name, $old_theme ) {
	if ( $old_theme && $old_theme->stylesheet ) {
		switch_theme( $old_theme->stylesheet );
	} else {
		switch_theme( WP_DEFAULT_THEME );
	}
	unset( $_GET['activated'] ); // phpcs:ignore WordPress.Security.NonceVerification.Recommended
	add_action( 'admin_notices', 'goodtailor_upgrade_notice' );
}
add_action( 'after_switch_theme', 'goodtailor_switch_theme', 10, 2 );

/**
 * Message text which will be displayed if PHP or WordPress are outdated.
 */
function goodtailor_get_upgrade_message() {
	/* translators: 1: is current PHP version, 2: is current WordPress version */
	return sprintf( __( 'GoodTailor Theme requires at least PHP 5.3 and WordPress 4.5. You are running PHP %1$s and WordPress %2$s. Please upgrade and try again.', 'goodtailor' ), phpversion(), $GLOBALS['wp_version'] );
}

/**
 * Adds a message for unsuccessful theme switch.
 */
function goodtailor_upgrade_notice() {
	printf( '<div class="error"><p>%s</p></div>', esc_html( goodtailor_get_upgrade_message() ) );
}

/**
 * Prevents the Customizer from being loaded if PHP or WordPress version is not compatible.
 */
function goodtailor_customize() {
	wp_die( esc_html( goodtailor_get_upgrade_message() ), '', array( 'back_link' => true ) );
}
add_action( 'load-customize.php', 'goodtailor_customize' );

/**
 * Prevents the Theme Preview from being loaded if PHP or WordPress version is not compatible.
 */
function goodtailor_preview() {
	if ( isset( $_GET['preview'] ) ) { // phpcs:ignore WordPress.CSRF.NonceVerification.NoNonceVerification
		wp_die( esc_html( goodtailor_get_upgrade_message() ) );
	}
}
add_action( 'template_redirect', 'goodtailor_preview' );
