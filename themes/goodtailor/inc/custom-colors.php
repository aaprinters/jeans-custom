<?php
/**
 * Custom colors CSS.
 *
 * @package GoodTailor
 */

/**
 * Hooks to wp_head to print CSS for user defined colors.
 */
function goodtailor_print_custom_colors() {
	goodtailor_custom_color_css_wrap( 'primary_color' );
	goodtailor_custom_color_css_wrap( 'primary_color_light' );
	goodtailor_primary_button_colors_css();
}
add_action( 'wp_head', 'goodtailor_print_custom_colors' );

/**
 * Prints the style tag and CSS for the given user defined color.
 */
function goodtailor_custom_color_css_wrap( $color_name ) {
	global $goodtailor_defaults;
	$color = get_theme_mod( $color_name, $goodtailor_defaults[ $color_name ] );
	if ( $goodtailor_defaults[ $color_name ] === $color && ! is_customize_preview() ) {
		return;
	}
	?>
	<style id="<?php echo 'goodtailor-custom-' . esc_attr( $color_name ); ?>" <?php if ( is_customize_preview() ) { echo 'data-current-color="' . esc_attr( $color ) . '"'; } ?>>
		<?php do_action( 'goodtailor_print_custom_' . $color_name . '_css', $color ); ?>
	</style>
	<?php
}

/**
 * Prints the CSS for user defined Primary color.
 */
function goodtailor_print_primary_color_css( $primary_color ) {
	echo '
.btn:not(.btn-primary):hover,
.search-form .search-submit:hover,
.post-password-form input[type="submit"]:hover,
.widget:not(.widget_text) a:hover,
.feature-box--style-1 .feature-box-icon,
.feature-box--style-2 .feature-box-icon,
.testimonial-item::before,
.article-card-title:hover,
.article-card-meta a:hover,
.entry-title a:hover,
.entry-meta a:hover,
.entry-tags a:hover,
.comments-nav a:hover,
.comments-nav a:focus,
.comment-date a:hover,
.comment-actions a:hover,
.header-info-block-icon,
.woocommerce a.button:hover,
.woocommerce button.button:hover,
.woocommerce input.button:hover,
.woocommerce #respond input#submit:hover,
.woocommerce a.added_to_cart:hover,
.woocommerce #review_form #respond .comment-form-rating .stars a,
.woocommerce .star-rating,
.woocommerce .star-rating::before {
	color: ' . esc_html( $primary_color ) . ';
}

.btn-primary,
.btn-primary:hover, .btn-primary:focus,
.header-info-block-icon.header-info-block-icon--circled,
.woocommerce span.onsale,
.woocommerce div.product form.cart .button,
.woocommerce div.product form.cart .button:hover,
.woocommerce div.product form.cart .button:focus,
.woocommerce a.button.alt,
.woocommerce button.button.alt,
.woocommerce input.button.alt,
.woocommerce #respond input#submit.alt,
.woocommerce-cart .wc-proceed-to-checkout a.checkout-button,
.woocommerce-checkout .wc-proceed-to-checkout a.checkout-button,
#add_payment_method .wc-proceed-to-checkout a.checkout-button,
.woocommerce-cart .wc-proceed-to-checkout a.checkout-button:hover,
.woocommerce-cart .wc-proceed-to-checkout a.checkout-button:focus,
.woocommerce-checkout .wc-proceed-to-checkout a.checkout-button:hover,
.woocommerce-checkout .wc-proceed-to-checkout a.checkout-button:focus,
#add_payment_method .wc-proceed-to-checkout a.checkout-button:hover,
#add_payment_method .wc-proceed-to-checkout a.checkout-button:focus,
.woocommerce-cart #payment #place_order,
.woocommerce-checkout #payment #place_order,
#add_payment_method #payment #place_order,
.woocommerce-cart #payment #place_order:hover,
.woocommerce-cart #payment #place_order:focus,
.woocommerce-checkout #payment #place_order:hover,
.woocommerce-checkout #payment #place_order:focus,
#add_payment_method #payment #place_order:hover,
#add_payment_method #payment #place_order:focus {
	background-color: ' . esc_html( $primary_color ) . ';
}

.text-primary {
	color: ' . esc_html( $primary_color ) . ' !important;
}

.bg-primary {
	background-color: ' . esc_html( $primary_color ) . ' !important;
}

@media (max-width: 991px) {
	.primary-navigation ul li.current-menu-item > a {
		color: ' . esc_html( $primary_color ) . ';
	}
}

@media (min-width: 992px) {
	.service-star:hover .service-star-title,
	.service-card:hover .service-card-title,
	.primary-navigation ul li > a:hover,
	.primary-navigation ul li.current-menu-item > a,
	.primary-navigation ul li.current-menu-ancestor > a,
	.woocommerce ul.products li.product-category:hover .woocommerce-loop-category__title,
	.woocommerce ul.products li.product-category:hover .woocommerce-loop-category__title .count {
		color: ' . esc_html( $primary_color ) . ';
	}
	.woocommerce ul.products li.product:hover .button {
		background-color: ' . esc_html( $primary_color ) . ';
	}
}
';
}
add_action( 'goodtailor_print_custom_primary_color_css', 'goodtailor_print_primary_color_css' );

/**
 * Prints the CSS for lightened by 10% user defined Primary color.
 */
function goodtailor_print_primary_color_light_css( $primary_color_light ) {
	echo '
blockquote::before,
.icon-list li i:first-child {
	color: ' . esc_html( $primary_color_light ) . ';
}

.bt-carousel .owl-dot.active,
.bt-carousel button.owl-dot.active {
	border-color: ' . esc_html( $primary_color_light ) . ';
	background-color: ' . esc_html( $primary_color_light ) . ';
}
';
}
add_action( 'goodtailor_print_custom_primary_color_light_css', 'goodtailor_print_primary_color_light_css' );

/**
 * Prints special CSS for custom Primary Button colors.
 */
function goodtailor_primary_button_colors_css() {
	global $goodtailor_defaults;
	$color_1 = get_theme_mod( 'primary_button_color_1', $goodtailor_defaults['primary_button_color_1'] );
	$color_2 = get_theme_mod( 'primary_button_color_2', $goodtailor_defaults['primary_button_color_2'] );
	if ( $goodtailor_defaults['primary_button_color_1'] === $color_1 && $goodtailor_defaults['primary_button_color_2'] === $color_2
		&& ! is_customize_preview() ) {
		return;
	}
	?>

	<style id="goodtailor-custom-primary-button-colors"
		<?php if ( is_customize_preview() ) { echo 'data-current-color-1="' . esc_attr( $color_1 ) . '" data-current-color-2="' . esc_attr( $color_2 ) . '"'; } ?>>
		.btn-primary {
			background-image: url("data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg'><rect width='100%' height='100%' rx='24' ry='24' fill='none' stroke='%23fff' stroke-dasharray='6,5' stroke-width='2' stroke-opacity='0.7' /></svg>"), -webkit-gradient(linear, left top, left bottom, from(<?php echo esc_html( $color_1 ); ?> /*1*/), color-stop(50%, <?php echo esc_html( $color_1 ); ?> /*1*/), to(<?php echo esc_html( $color_2 ); ?> /*2*/));
			background-image: url("data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg'><rect width='100%' height='100%' rx='24' ry='24' fill='none' stroke='%23fff' stroke-dasharray='6,5' stroke-width='2' stroke-opacity='0.7' /></svg>"), linear-gradient(<?php echo esc_html( $color_1 ); ?> /*1*/ 0%, <?php echo esc_html( $color_1 ); ?> /*1*/ 50%, <?php echo esc_html( $color_2 ); ?> /*2*/ 100%);
		}

		.btn-primary.btn-large {
			background-image: url("data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg'><rect width='100%' height='100%' rx='29' ry='29' fill='none' stroke='%23fff' stroke-dasharray='6,5' stroke-width='2' stroke-opacity='0.7' /></svg>"), -webkit-gradient(linear, left top, left bottom, from(<?php echo esc_html( $color_1 ); ?> /*1*/), color-stop(50%, <?php echo esc_html( $color_1 ); ?> /*1*/), to(<?php echo esc_html( $color_2 ); ?> /*2*/));
			background-image: url("data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg'><rect width='100%' height='100%' rx='29' ry='29' fill='none' stroke='%23fff' stroke-dasharray='6,5' stroke-width='2' stroke-opacity='0.7' /></svg>"), linear-gradient(<?php echo esc_html( $color_1 ); ?> /*1*/ 0%, <?php echo esc_html( $color_1 ); ?> /*1*/ 50%, <?php echo esc_html( $color_2 ); ?> /*2*/ 100%);
		}

		.btn-primary.btn-small {
			background-image: url("data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg'><rect width='100%' height='100%' rx='17' ry='17' fill='none' stroke='%23fff' stroke-dasharray='6,5' stroke-width='2' stroke-opacity='0.7' /></svg>"), -webkit-gradient(linear, left top, left bottom, from(<?php echo esc_html( $color_1 ); ?> /*1*/), color-stop(50%, <?php echo esc_html( $color_1 ); ?> /*1*/), to(<?php echo esc_html( $color_2 ); ?> /*2*/));
			background-image: url("data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg'><rect width='100%' height='100%' rx='17' ry='17' fill='none' stroke='%23fff' stroke-dasharray='6,5' stroke-width='2' stroke-opacity='0.7' /></svg>"), linear-gradient(<?php echo esc_html( $color_1 ); ?> /*1*/ 0%, <?php echo esc_html( $color_1 ); ?> /*1*/ 50%, <?php echo esc_html( $color_2 ); ?> /*2*/ 100%);
		}

		.woocommerce div.product form.cart .button,
		.woocommerce-cart #payment #place_order,
		.woocommerce-checkout #payment #place_order,
		#add_payment_method #payment #place_order {
			background-image: url("data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg'><rect width='100%' height='100%' rx='19' ry='19' fill='none' stroke='%23fff' stroke-dasharray='6,5' stroke-width='2' stroke-opacity='0.7' /></svg>"), -webkit-gradient(linear, left top, left bottom, from(<?php echo esc_html( $color_1 ); ?> /*1*/), color-stop(50%, <?php echo esc_html( $color_1 ); ?> /*1*/), to(<?php echo esc_html( $color_2 ); ?> /*2*/));
			background-image: url("data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg'><rect width='100%' height='100%' rx='19' ry='19' fill='none' stroke='%23fff' stroke-dasharray='6,5' stroke-width='2' stroke-opacity='0.7' /></svg>"), linear-gradient(<?php echo esc_html( $color_1 ); ?> /*1*/ 0%, <?php echo esc_html( $color_1 ); ?> /*1*/ 50%, <?php echo esc_html( $color_2 ); ?> /*2*/ 100%);
		}

		.woocommerce-cart .wc-proceed-to-checkout a.checkout-button,
		.woocommerce-checkout .wc-proceed-to-checkout a.checkout-button,
		#add_payment_method .wc-proceed-to-checkout a.checkout-button {
			background-image: url("data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg'><rect width='100%' height='100%' rx='25' ry='25' fill='none' stroke='%23fff' stroke-dasharray='6,5' stroke-width='2' stroke-opacity='0.7' /></svg>"), -webkit-gradient(linear, left top, left bottom, from(<?php echo esc_html( $color_1 ); ?> /*1*/), color-stop(50%, <?php echo esc_html( $color_1 ); ?> /*1*/), to(<?php echo esc_html( $color_2 ); ?> /*2*/));
			background-image: url("data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg'><rect width='100%' height='100%' rx='25' ry='25' fill='none' stroke='%23fff' stroke-dasharray='6,5' stroke-width='2' stroke-opacity='0.7' /></svg>"), linear-gradient(<?php echo esc_html( $color_1 ); ?> /*1*/ 0%, <?php echo esc_html( $color_1 ); ?> /*1*/ 50%, <?php echo esc_html( $color_2 ); ?> /*2*/ 100%);
		}

		.woocommerce a.button.alt:hover,
		.woocommerce button.button.alt:hover,
		.woocommerce input.button.alt:hover,
		.woocommerce #respond input#submit.alt:hover {
			background-color: <?php echo esc_html( $color_2 ); ?> /*2*/;
		}
	</style>

	<?php
}
