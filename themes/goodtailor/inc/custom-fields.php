<?php
/**
 * Custom fields for posts and pages
 *
 * @package GoodTailor
 */

/**
 * Use Carbon Fields plugin API to add custom fields meta boxes.
 */
function goodtailor_register_carbon_fields() {
	Carbon_Fields\Container::make( 'post_meta', esc_html__( 'Page Settings', 'goodtailor' ) )
		->show_on_post_type( array( 'post', 'page', 'service' ) )
		->set_priority( 'default' )
		->add_fields( array(
			Carbon_Fields\Field::make( 'select', 'goodtailor_header_layout', esc_html__( 'Override Header Layout?', 'goodtailor' ) )
				->add_options( array(
					''                   => esc_html__( 'Don\'t Override', 'goodtailor' ),
					'layout_standard'    => esc_html__( 'Standard', 'goodtailor' ),
					'layout_transparent' => esc_html__( 'Transparent', 'goodtailor' ),
				) ),
			Carbon_Fields\Field::make( 'select', 'goodtailor_header_color_style', esc_html__( 'Override Header Color Style?', 'goodtailor' ) )
				->add_options( array(
					''         => esc_html__( 'Don\'t Override', 'goodtailor' ),
					'bg_light' => esc_html__( 'Dark text on Light background', 'goodtailor' ),
					'bg_dark'  => esc_html__( 'Light text on Dark background', 'goodtailor' ),
				) ),
			Carbon_Fields\Field::make( 'select', 'goodtailor_show_page_header', esc_html__( 'Show Page Header?', 'goodtailor' ) )
				->add_options( array(
					''     => esc_html__( 'Default', 'goodtailor' ),
					'show' => esc_html__( 'Show', 'goodtailor' ),
					'hide' => esc_html__( 'Hide', 'goodtailor' ),
				) ),
			Carbon_Fields\Field::make( 'select', 'goodtailor_show_breadcrumbs', esc_html__( 'Show Breadcrumbs?', 'goodtailor' ) )
				->add_options( array(
					''     => esc_html__( 'Default', 'goodtailor' ),
					'show' => esc_html__( 'Show', 'goodtailor' ),
					'hide' => esc_html__( 'Hide', 'goodtailor' ),
				) )
				->set_conditional_logic( array(
					array(
						'field'   => 'goodtailor_show_page_header',
						'value'   => 'hide',
						'compare' => '!=',
					),
				) ),
		) );
}
add_action( 'carbon_register_fields', 'goodtailor_register_carbon_fields' );
