<?php
/**
 * Revolution Slider plugin integration.
 *
 * @package GoodTailor
 */

/**
 * Enqueue Revolution Slider Editor Styles & Scripts
 */
function goodtailor_enqueue_revslider_admin_scripts() {
	// Check if on Revolution Slider Admin Screen
	if ( 'toplevel_page_revslider' === get_current_screen()->id ) {
		wp_enqueue_style( 'goodtailor-rs-editor-style', get_template_directory_uri() . '/assets/css/editor-style.css', array(), GOODTAILOR_THEME_VERSION );
	}
}
add_action( 'admin_enqueue_scripts', 'goodtailor_enqueue_revslider_admin_scripts' );
