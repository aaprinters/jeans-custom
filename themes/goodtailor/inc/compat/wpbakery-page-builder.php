<?php
/**
 * WPBakery Pager Builder (formerly Visual Composer) plugin integration.
 *
 * @package GoodTailor
 */

/**
 * Customize VC settings
 */
function goodtailor_vc_before_init() {
	vc_set_as_theme();
	vc_set_default_editor_post_types( array( 'page', 'service' ) );
}
add_action( 'vc_before_init', 'goodtailor_vc_before_init' );

/**
 * Add additional styles for VC Backend Editor.
 */
function goodtailor_add_vc_backend_editor_style() {
	wp_enqueue_style( 'goodtailor-vc-editor-style', get_template_directory_uri() . '/assets/css/editor-style.css', array(), GOODTAILOR_THEME_VERSION );
}
add_action( 'vc_backend_editor_enqueue_js_css', 'goodtailor_add_vc_backend_editor_style' );

/**
 * Disable loading Themify icons CSS by our plugin, because necessary styles are
 * already included in the theme.
 */
add_filter( 'goodtailor_vc_enqueue_themify_icons_css', '__return_false' );
