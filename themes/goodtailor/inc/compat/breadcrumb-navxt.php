<?php
/**
 * Breadcrumb NavXT plugin itegration.
 *
 * @package GoodTailor
 */

/**
 * Remove the title of the current post/page from the breadcrumb trail to avoid duplication,
 * since the breadcrumbs are displayed right under the page title.
 */
function goodtailor_bcn_remove_post_title( $trail ) {
	if ( ! is_front_page() && ! empty( $trail->breadcrumbs ) && get_the_ID() === $trail->breadcrumbs[0]->get_id() ) {
		unset( $trail->breadcrumbs[0] );
		$trail->breadcrumbs = array_values( $trail->breadcrumbs );
	}
}
add_action( 'bcn_after_fill', 'goodtailor_bcn_remove_post_title' );
