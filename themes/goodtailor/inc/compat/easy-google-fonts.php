<?php
/**
 * Easy Google Fonts plugin integration to give more powerful controls for typography settings.
 *
 * @package GoodTailor
 */

/**
 * Change the priority of Typography section in the Customizer so that it appears
 * below Theme options section.
 */
function goodtailor_tt_font_get_panels( $panels ) {
	if ( isset( $panels['tt_font_typography_panel'] ) ) {
		$panels['tt_font_typography_panel']['priority'] = 131; // After Header Info Blocks
	}
	return $panels;
}
add_filter( 'tt_font_get_panels', 'goodtailor_tt_font_get_panels' );

/**
 * Add additional typography options to allow customization of some theme elements.
 */
function goodtailor_tt_font_get_option_parameters( $options ) {
	$theme_options = array(
		'goodtailor_tt_body'     => array(
			'name'       => 'goodtailor_tt_body',
			'title'      => esc_html__( 'Body', 'goodtailor' ),
			'properties' => array( 'selector' => apply_filters( 'goodtailor_tt_body', 'body' ) ),
		),
		'goodtailor_tt_headings' => array(
			'name'       => 'goodtailor_tt_headings',
			'title'      => esc_html__( 'Headings', 'goodtailor' ),
			'properties' => array( 'selector' => apply_filters( 'goodtailor_tt_headings', 'h1, h2, h3, h4, h5, h6, .h1, .h2, .h3, .h4, .h5, .h6' ) ),
		),
	);
	$options = array_merge( $theme_options, $options );
	return $options;
}
add_filter( 'tt_font_get_option_parameters', 'goodtailor_tt_font_get_option_parameters' );

/**
 * Override CSS selector for h1-h6 headings
 *
 * The missing h in 'h1' is intentional, because that's how the filter is called in EGF plugin.
 */
foreach ( array( '1', 'h2', 'h3', 'h4', 'h5', 'h6' ) as $level ) {
	add_filter( 'tt_default_heading_' . $level, function() use ( $level ) {
		return sprintf( 'h%1$d, .h%1$d', str_replace( 'h', '', $level ) );
	} );
}
