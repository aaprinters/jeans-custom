<?php
/**
 * Theme default values.
 *
 * @package GoodTailor
 */

/**
 * A global array with theme default values.
 */
global $goodtailor_defaults;

if ( ! isset( $goodtailor_defaults ) ) {
	$goodtailor_defaults = array(
		'header_layout'          => 'layout_standard',
		'header_color_style'     => 'bg_light',
		'show_breadcrumbs'       => 'show',
		'primary_color'          => '#c71a1e',
		'primary_color_light'    => '#e43034',
		'primary_button_color_1' => '#de1d21',
		'primary_button_color_2' => '#a31519',
		'header_bg_color'        => 'rgba(24,25,36,1)',
		'footer_bg_color'        => 'rgba(255,255,255,1)',
		'footer_bottom_bg_color' => 'rgba(24,25,36,1)',
		/* translators: 1: is a link to the author portfolio page, 2: is the author name */
		'footer_tagline'         => sprintf( __( '&copy; 2017 GoodTailor WordPress Theme by <a href="%1$s">%2$s</a>', 'goodtailor' ), 'https://themeforest.net/user/brotherstheme', 'BrothersTheme' ),
	);
}

/**
 * Retrieve theme default value from the global array.
 */
function goodtailor_get_default( $key ) {
	global $goodtailor_defaults;
	return $goodtailor_defaults[ $key ];
}
