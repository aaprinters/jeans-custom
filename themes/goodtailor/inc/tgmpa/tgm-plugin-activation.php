<?php
/**
 * TGM Plugin Activation integration to allow easy installation of recommended plugins.
 *
 * @package GoodTailor
 */

require_once get_template_directory() . '/inc/tgmpa/class-tgm-plugin-activation.php';

/**
 * Provide a list of recommended plugins.
 */
function goodtailor_register_recommended_plugins() {
	$plugins = array(
		array(
			'name'     => esc_html__( 'GoodTailor Custom Post Types & Shortcodes Plugin', 'goodtailor' ),
			'slug'     => 'goodtailor-plugin',
			'source'   => get_template_directory() . '/inc/tgmpa/plugins/goodtailor-plugin.zip',
			'required' => false,
			'version'  => '1.5.2',
		),
		array(
			'name'     => esc_html__( 'Breadcrumb NavXT', 'goodtailor' ),
			'slug'     => 'breadcrumb-navxt',
			'required' => false,
		),
		array(
			'name'     => esc_html__( 'Carbon Fields', 'goodtailor' ),
			'slug'     => 'carbon-fields',
			'source'   => get_template_directory() . '/inc/tgmpa/plugins/carbon-fields.zip',
			'required' => false,
			'version'  => '1.6.0',
		),
		array(
			'name'     => esc_html__( 'Contact Form 7', 'goodtailor' ),
			'slug'     => 'contact-form-7',
			'required' => false,
		),
		array(
			'name'     => esc_html__( 'Easy Google Fonts', 'goodtailor' ),
			'slug'     => 'easy-google-fonts',
			'required' => false,
		),
		array(
			'name'     => esc_html__( 'WPBakery Visual Composer', 'goodtailor' ),
			'slug'     => 'js_composer',
			'source'   => get_template_directory() . '/inc/tgmpa/plugins/js_composer.zip',
			'required' => false,
			'version'  => '6.2.0',
		),
		array(
			'name'     => esc_html__( 'MailChimp for WordPress', 'goodtailor' ),
			'slug'     => 'mailchimp-for-wp',
			'required' => false,
		),
		array(
			'name'     => esc_html__( 'One Click Demo Import', 'goodtailor' ),
			'slug'     => 'one-click-demo-import',
			'required' => false,
		),
		array(
			'name'     => esc_html__( 'Slider Revolution', 'goodtailor' ),
			'slug'     => 'revslider',
			'source'   => get_template_directory() . '/inc/tgmpa/plugins/revslider.zip',
			'required' => false,
			'version'  => '6.2.2',
		),
		array(
			'name'     => esc_html__( 'WooCommerce', 'goodtailor' ),
			'slug'     => 'woocommerce',
			'required' => false,
		),
	);

	tgmpa( $plugins, array( 'is_automatic' => true ) );
}
add_action( 'tgmpa_register', 'goodtailor_register_recommended_plugins' );
