<?php
/**
 * Customize API: GoodTailor_Customize_RGBA_Color_Control class
 *
 * @package GoodTailor
 */

/**
 * Customize RGBA Color Control class.
 */
class GoodTailor_Customize_RGBA_Color_Control extends WP_Customize_Control {
	/**
	 * Type.
	 *
	 * @var string
	 */
	public $type = 'color_rgba';

	/**
	 * Enqueue scripts/styles for the color picker.
	 */
	public function enqueue() {
		wp_enqueue_style( 'goodtailor-customize-rgba-color-control',
			get_template_directory_uri() . '/assets/customizer/rgba-color-control.css',
			array( 'customize-controls', 'wp-color-picker' ),
			GOODTAILOR_THEME_VERSION
		);
		wp_enqueue_script( 'goodtailor-customize-rgba-color-control',
			get_template_directory_uri() . '/assets/customizer/rgba-color-control.js',
			array( 'customize-controls', 'wp-color-picker' ),
			GOODTAILOR_THEME_VERSION,
			true
		);
	}

	/**
	 * Refresh the parameters passed to the JavaScript via JSON.
	 */
	public function to_json() {
		parent::to_json();
		$this->json['defaultValue'] = $this->setting->default;
	}

	/**
	 * Don't render the control content from PHP, as it's rendered via JS on load.
	 */
	public function render_content() {}

	/**
	 * Render a JS template for the content of the color picker control.
	 */
	public function content_template() {
		?>
		<# var defaultHex = goodtailor.customize.rgbaGetHex( data.defaultValue );
		if ( defaultHex ) {
			defaultHex = ' data-default-color=' + defaultHex; // Quotes added automatically.
		} else {
			defaultHex = '';
		} #>
		<label>
			<# if ( data.label ) { #>
				<span class="customize-control-title">{{{ data.label }}}</span>
			<# } #>
			<# if ( data.description ) { #>
				<span class="description customize-control-description">{{{ data.description }}}</span>
			<# } #>
			<div class="customize-control-content">
				<input class="color-picker-hex" type="text" maxlength="7" placeholder="<?php esc_attr_e( 'Hex Value', 'goodtailor' ); ?>" {{ defaultHex }} />
			</div>
		</label>
		<label>
			<span class="alpha-control-title">Alpha</span>
			<div class="alpha-controls">
				<input class="alpha-number" type="number" min="0" max="100" step="1" value="100" />
				<input class="alpha-slider" type="range" min="0" max="100" step="1" value="100" />
			</div>
		</label>
		<?php
	}
}
