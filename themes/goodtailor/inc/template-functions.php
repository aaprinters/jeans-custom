<?php
/**
 * Additional features to allow styling of the templates.
 *
 * @package GoodTailor
 */

/**
 * Adds custom classes to the array of body classes.
 */
function goodtailor_body_classes( $classes ) {
	// Add class if sidebar is used
	if ( function_exists( 'is_woocommerce' ) && is_woocommerce() ) {
		if ( is_active_sidebar( 'shop-sidebar-1' ) ) {
			$classes[] = 'has-sidebar';

			if ( is_shop() || is_product_taxonomy() || is_product_category() || is_product_tag() ) {
				$classes[] = 'columns-3';
			}
		}
	} elseif ( is_active_sidebar( 'sidebar-1' ) && ( ! is_singular() || is_singular( 'post' ) ) && ! is_404() ) {
		$classes[] = 'has-sidebar';
	}
	// Add class if we're viewing the Customizer for easier styling of theme options.
	if ( is_customize_preview() ) {
		$classes[] = 'goodtailor-customizer';
	}
	// Add class if page header is hidden on the page.
	if ( ! goodtailor_show_page_header() ) {
		$classes[] = 'hide-page-header';
	}
	// Add class if Site Title and Tagline are hidden.
	if ( goodtailor_is_site_branding_text_hidden() ) {
		$classes[] = 'hide-site-branding-text';
	}
	return $classes;
}
add_filter( 'body_class', 'goodtailor_body_classes' );

/**
 * Replace .no-js class with .js in root element to indicate JS support.
 */
function goodtailor_has_js() {
	?>
	<script>jQuery( 'html' ).removeClass( 'no-js' ).addClass( 'js' );</script>
	<?php
}
add_filter( 'wp_head', 'goodtailor_has_js' );

/**
 * Retrieves the list of classes for the site header.
 */
function goodtailor_get_header_class( $class = '' ) {
	$classes = array();

	if ( $class ) {
		if ( ! is_array( $class ) ) {
			$class = preg_split( '#\s+#', $class );
		}
		$classes = $class;
	} else {
		// Ensure that we always coerce class to being an array.
		$class = array();
	}

	$classes[] = 'site-header';
	$layout = goodtailor_get_setting( 'header_layout' );
	if ( 'layout_standard' !== $layout ) {
		$classes[] = 'site-header--' . $layout;
	}
	$color_style = goodtailor_get_setting( 'header_color_style' );
	if ( 'bg_light' !== $color_style ) {
		$classes[] = 'site-header--' . $color_style;
	}

	/**
	 * Filters the list of CSS classes for the header.
	 *
	 * @param array $classes An array of header classes.
	 * @param array $class   An array of additional classes added to the header.
	 */
	$classes = apply_filters( 'goodtailor_header_class', $classes, $class );
	return array_unique( $classes );
}

/**
 * Filter the custom logo to add the light logo img tag.
 */
function goodtailor_get_custom_logo( $html ) {
	// Check if the light logo was set in the Customizer
	$light_logo_id = get_theme_mod( 'custom_logo_light' );
	if ( ! $light_logo_id ) {
		return $html;
	}

	$light_logo_attr = array(
		'class'    => 'custom-logo custom-logo-light',
		'itemprop' => 'logo',
	);

	$image_alt = get_post_meta( $light_logo_id, '_wp_attachment_image_alt', true );
	if ( empty( $image_alt ) ) {
		$light_logo_attr['alt'] = get_bloginfo( 'name', 'display' );
	}

	if ( ! get_theme_mod( 'custom_logo' ) ) {
		return sprintf( '<a href="%1$s" class="custom-logo-link" rel="home" itemprop="url">%2$s</a>',
			esc_url( home_url( '/' ) ),
			wp_get_attachment_image( $light_logo_id, 'full', false, $light_logo_attr )
		);
	}

	return str_replace( '</a>', wp_get_attachment_image( $light_logo_id, 'full', false, $light_logo_attr ) . '</a>', $html );
}
add_filter( 'get_custom_logo', 'goodtailor_get_custom_logo' );

/**
 * Include SVG definitions which can be referenced in HTML.
 */
function goodtailor_include_svg_defs() {
	$svg_defs = get_template_directory() . '/assets/images/svg-defs.svg';
	if ( file_exists( $svg_defs ) ) {
		require_once $svg_defs;
	}
}
add_filter( 'wp_footer', 'goodtailor_include_svg_defs', 9999 );

/**
 * A "before" part of the wrapper of the main content on pages which were not built using
 * Visual Composer.
 *
 * The reason we don't want to wrap VC pages is because we don't want to be adding the
 * extra padding or margin that comes with the wrapper. This is because it's easier to add
 * those paddings or margins via Visual Composer itself.
 */
function goodtailor_page_main_wrap_before() {
	if ( ! goodtailor_is_vc_active_on_page() ) {
		echo '<div class="container">';
		echo '<div class="main-wrap">';
	}
}
add_action( 'goodtailor_page_main_before', 'goodtailor_page_main_wrap_before' );
add_action( 'goodtailor_single_main_before', 'goodtailor_page_main_wrap_before' );

/**
 * An "after" part of the wrapper of the main content on pages which were not built using
 * Visual Composer.
 */
function goodtailor_page_main_wrap_after() {
	if ( ! goodtailor_is_vc_active_on_page() ) {
		echo '</div>'; // .main-wrap
		echo '</div>'; // .container
	}
}
add_action( 'goodtailor_page_main_after', 'goodtailor_page_main_wrap_after' );
add_action( 'goodtailor_single_main_after', 'goodtailor_page_main_wrap_after' );

/**
 * Returns i18n friendly Google Fonts url.
 * As described in https://themeshaper.com/2014/08/13/how-to-add-google-fonts-to-wordpress-themes/
 */
function goodtailor_google_fonts_url() {
	/* translators: If there are characters in your language that are not
	 * supported by Vidaloka, translate this to 'off'. Do not translate
	 * into your own language.
	 */
	$vidaloka = _x( 'on', 'Vidaloka font: on or off', 'goodtailor' );
	/* translators: If there are characters in your language that are not
	 * supported by Open Sans, translate this to 'off'. Do not translate
	 * into your own language.
	 */
	$open_sans = _x( 'on', 'Open Sans font: on or off', 'goodtailor' );
	if ( 'off' === $vidaloka && 'off' === $open_sans ) {
		return '';
	}
	$fonts = array();
	if ( 'off' !== $vidaloka ) {
		$fonts[] = 'Vidaloka:400';
	}
	if ( 'off' !== $open_sans ) {
		$fonts[] = 'Open Sans:300,300i,400,400i,700,700i';
	}
	$query_args = array(
		'family' => rawurlencode( implode( '|', $fonts ) ),
		'subset' => rawurlencode( 'latin,latin-ext' ),
	);
	return esc_url_raw( add_query_arg( $query_args, '//fonts.googleapis.com/css' ) );
}

/**
 * Customize the comment form rendering function arguments.
 */
function goodtailor_comment_form_defaults( $defaults ) {
	$commenter = wp_get_current_commenter();
	$req = get_option( 'require_name_email' );
	$defaults['fields']['author'] = '<p class="comment-form-author"><label for="author" class="screen-reader-text">' . esc_html__( 'Name', 'goodtailor' ) . ( $req ? ' <span class="required">*</span>' : '' ) . '</label> ' .
		'<input id="author" name="author" type="text" placeholder="' . esc_attr__( 'Name', 'goodtailor' ) . ( $req ? ' *' : '' ) . '" value="' . esc_attr( $commenter['comment_author'] ) . '" size="30" maxlength="245"' . ( $req ? " required='required'" : '' ) . ' /></p>';
	$defaults['fields']['email'] = '<p class="comment-form-email"><label for="email" class="screen-reader-text">' . esc_html__( 'Email', 'goodtailor' ) . ( $req ? ' <span class="required">*</span>' : '' ) . '</label> ' .
		'<input id="email" name="email" type="email" placeholder="' . esc_attr__( 'Email', 'goodtailor' ) . ( $req ? ' *' : '' ) . '" value="' . esc_attr( $commenter['comment_author_email'] ) . '" size="30" maxlength="100" aria-describedby="email-notes"' . ( $req ? " required='required'" : '' ) . ' /></p>';
	$defaults['fields']['url'] = '<p class="comment-form-url"><label for="url" class="screen-reader-text">' . esc_html__( 'Website', 'goodtailor' ) . '</label> ' .
		'<input id="url" name="url" type="url" placeholder="' . esc_attr__( 'Website', 'goodtailor' ) . '" value="' . esc_attr( $commenter['comment_author_url'] ) . '" size="30" maxlength="200" /></p>';
	$defaults['comment_field'] = '<p class="comment-form-comment"><label for="comment" class="screen-reader-text">' . esc_html_x( 'Comment', 'noun', 'goodtailor' ) . '</label> <textarea id="comment" name="comment" placeholder="' . esc_attr_x( 'Comment', 'noun', 'goodtailor' ) . '" cols="45" rows="6" maxlength="65525" required="required"></textarea></p>';
	$defaults['class_submit'] = 'submit btn';
	return $defaults;
}
add_filter( 'comment_form_defaults', 'goodtailor_comment_form_defaults' );

/**
 * Returns a CSS string with style overrides based on customized theme options.
 */
function goodtailor_get_inline_style() {
	$css = '';
	$header_bg_color = get_theme_mod( 'header_bg_color' );
	if ( $header_bg_color ) {
		$css .= '.page-header-title-wrap { background-color: ' . $header_bg_color . '; } ';
	}
	$footer_bg_image_id = get_theme_mod( 'footer_bg_image_id' );
	if ( $footer_bg_image_id ) {
		$footer_bg_image_url = wp_get_attachment_image_url( $footer_bg_image_id, 'full' );
		$css .= '.site-footer { background-image: url(' . esc_url( $footer_bg_image_url ) . '); } ';
	}
	$footer_bg_color = get_theme_mod( 'footer_bg_color' );
	if ( $footer_bg_color ) {
		$css .= '.footer-top-wrap { background-color: ' . $footer_bg_color . '; } ';
	}
	$footer_bottom_bg_color = get_theme_mod( 'footer_bottom_bg_color' );
	if ( $footer_bottom_bg_color ) {
		$css .= '.footer-bottom-wrap { background-color: ' . $footer_bottom_bg_color . '; } ';
	}
	return $css;
}

/**
 * A filter to allow some additional tags and attributes in the content which are not allowed
 * by default in WordPress.
 */
function goodtailor_wp_kses_allowed_html( $tags, $context ) {
	// <time></time>
	$tags['time'] = array( 'datetime' => true );
	// <span></span>
	if ( ! isset( $tags['span'] ) ) {
		$tags['span'] = array();
	}
	$tags['span']['class'] = true;
	return $tags;
}
add_filter( 'wp_kses_allowed_html', 'goodtailor_wp_kses_allowed_html', 10, 2 );

/**
 * A filter to allow skype: protocol in the links.
 */
function goodtailor_kses_allowed_protocols( $protocols ) {
	$protocols[] = 'skype';
	return $protocols;
}
add_filter( 'kses_allowed_protocols', 'goodtailor_kses_allowed_protocols' );

/**
 * A filter to customize the arguments passed to the WP Tag Cloud Widget generating function.
 */
function goodtailor_tag_cloud_args( $args ) {
	$args['smallest'] = $args['largest'] = 1;
	$args['unit'] = 'em';
	$args['separator'] = ', ';
	return $args;
}
add_filter( 'widget_tag_cloud_args', 'goodtailor_tag_cloud_args' );
add_filter( 'woocommerce_product_tag_cloud_widget_args', 'goodtailor_tag_cloud_args' );

/**
 * Finds an appropriate theme setting value by the key, first trying a post_meta for the
 * given key (prefixed with our theme slug) and if not found then trying a theme_mod.
 *
 * Useful for cases where we have a theme_mod which we want to be able to override on some
 * pages using post_meta.
 */
function goodtailor_get_setting( $key ) {
	$post_meta = get_post_meta( get_the_ID(), '_goodtailor_' . $key, true );
	if ( ! empty( $post_meta ) ) {
		return $post_meta;
	}
	global $goodtailor_defaults;
	return get_theme_mod( $key, $goodtailor_defaults[ $key ] );
}

/**
 * Check whether we should show the page header on the current page.
 */
function goodtailor_show_page_header() {
	if ( ! is_singular() ) {
		return ! is_front_page();
	}
	$show_page_header = get_post_meta( get_the_ID(), '_goodtailor_show_page_header', true );
	if ( is_front_page() ) {
		return 'show' === $show_page_header;
	}
	return 'hide' !== $show_page_header;
}

/**
 * Check if Visual Composer is activated on the current page.
 */
function goodtailor_is_vc_active_on_page() {
	$vc_status = get_post_meta( get_the_ID(), '_wpb_vc_js_status', true );
	return $vc_status && 'false' !== $vc_status;
}

/**
 * Query WooCommerce plugin activation.
 */
function goodtailor_is_woocommerce_activated() {
	return class_exists( 'WooCommerce' ) ? true : false;
}

/**
 * Retreives the footer tagline string which was set via the Customizer.
 */
function goodtailor_get_footer_tagline() {
	return goodtailor_html_entity_decode( get_theme_mod( 'footer_tagline', goodtailor_get_default( 'footer_tagline' ) ) );
}

/**
 * Converts a given date string into relative date format (e.g 10 minutes ago).
 */
function goodtailor_to_relative_date( $date_gmt ) {
	$now = time();
	$date = strtotime( $date_gmt );
	if ( $now - $date < MINUTE_IN_SECONDS ) {
		return _x( 'just now', 'relative time', 'goodtailor' );
	} else {
		/* translators: relative time format, e.g 10 minutes ago */
		return sprintf( _x( '%s ago', 'relative time', 'goodtailor' ), human_time_diff( $date, $now ) );
	}
}

/**
 * Checks that a user with the given user id is the author of the post.
 */
function goodtailor_is_post_author( $user_id, $post_id = null ) {
	return ( $user_id > 0 ) && ( $post = get_post( $post_id ) ) && ( $user_id === $post->post_author );
}

/**
 * Checks whether the given info block is active and should be displayed.
 */
function goodtailor_is_active_header_info_block( $id ) {
	return '' !== get_theme_mod( 'header_info_block_text_' . $id, '' );
}

/**
 * Checks whether Site Title and Tagline text were hidden via an option in the Customizer.
 */
function goodtailor_is_site_branding_text_hidden() {
	return 'blank' === get_header_textcolor();
}

/**
 * Initialize the $wp_filesystem global if it was not initialized before.
 */
function goodtailor_init_wp_filesystem() {
	if ( ! function_exists( 'WP_Filesystem' ) ) {
		require_once ABSPATH . 'wp-admin/includes/file.php';
	}
	global $wp_filesystem;
	if ( ! $wp_filesystem ) {
		WP_Filesystem();
	}
	return $wp_filesystem;
}

/**
 * Recursive search and replace.
 *  - If $data argument is a string then a regular string search and replace is performed.
 *  - If $data argument is an array then search and replace is called recursively for each value.
 *  - If $data argument is an object then search and replace is called recursively for each object var.
 *  - Otherwise the $data is returned unmodified.
 */
function goodtailor_recursive_replace( $from, $to, $data ) {
	if ( is_string( $data ) ) {
		$data = str_replace( $from, $to, $data );
	} elseif ( is_array( $data ) ) {
		$_tmp = array();
		foreach ( $data as $key => $value ) {
			$_tmp[ $key ] = goodtailor_recursive_replace( $from, $to, $value );
		}
		$data = $_tmp;
		unset( $_tmp );
	} elseif ( is_object( $data ) ) {
		$_tmp = $data;
		$props = get_object_vars( $data );
		foreach ( $props as $key => $value ) {
			$_tmp->$key = goodtailor_recursive_replace( $from, $to, $value );
		}
		$data = $_tmp;
		unset( $_tmp );
	}
	return $data;
}

/**
 * Encode HTML entities with UTF-8 default encoding in PHP 5.3 backwards
 * compatible way.
 */
function goodtailor_htmlspecialchars( $value ) {
	$flags = ENT_COMPAT;
	if ( defined( 'ENT_HTML401' ) ) {
		$flags |= ENT_HTML401; // phpcs:ignore PHPCompatibility.Constants.NewConstants.ent_html401Found
	}
	return htmlspecialchars( $value, $flags, 'UTF-8' );
}

/**
 * Decode HTML entities with UTF-8 default encoding in PHP 5.3 backwards
 * compatible way.
 */
function goodtailor_html_entity_decode( $value ) {
	$flags = ENT_COMPAT;
	if ( defined( 'ENT_HTML401' ) ) {
		$flags |= ENT_HTML401; // phpcs:ignore PHPCompatibility.Constants.NewConstants.ent_html401Found
	}
	return html_entity_decode( $value, $flags, 'UTF-8' );
}
