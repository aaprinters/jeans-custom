<?php
/**
 * Custom template tags for this theme
 *
 * @package GoodTailor
 */

if ( ! function_exists( 'goodtailor_header_class' ) ) :
	/**
	 * Display the classes for the site header.
	 */
	function goodtailor_header_class( $class = '' ) {
		echo 'class="' . esc_attr( join( ' ', goodtailor_get_header_class( $class ) ) ) . '"';
	}
endif;

if ( ! function_exists( 'goodtailor_site_branding_text' ) ) :
	/**
	 * Display Site Title and Tagline.
	 */
	function goodtailor_site_branding_text() {
		?>
		<div class="site-branding-text">
			<?php if ( is_front_page() ) : ?>
				<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
			<?php else : ?>
				<p class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></p>
			<?php endif; ?>
			<?php
			$description = get_bloginfo( 'description', 'display' );
			if ( '' !== $description ) :
				?>
				<p class="site-description"><?php echo esc_html( $description ); ?></p>
			<?php endif; ?>
		</div>
		<?php
	}
endif;

if ( ! function_exists( 'goodtailor_the_page_title' ) ) :
	/**
	 * Display the current page or post title.
	 */
	function goodtailor_the_page_title() {
		// 404 page
		if ( is_404() ) {
			esc_html_e( 'Oops! That page can&rsquo;t be found.', 'goodtailor' );
			return;
		}
		// Search pages
		if ( is_search() ) {
			if ( have_posts() ) {
				/* translators: Search results page title */
				esc_html_e( 'Search', 'goodtailor' );
			} else {
				esc_html_e( 'Nothing found', 'goodtailor' );
			}
			return;
		}
		// Single posts
		if ( is_singular( 'post' ) ) {
			echo get_the_title( get_option( 'page_for_posts', true ) ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
			return;
		}
		// WooCommerce pages
		if ( goodtailor_is_woocommerce_activated() && is_woocommerce() ) {
			woocommerce_page_title();
			return;
		}
		// Archive pages
		if ( is_archive() ) {
			the_archive_title();
			return;
		}
		// By default show the title
		single_post_title();
	}
endif;

if ( ! function_exists( 'goodtailor_breadcrumbs' ) ) :
	/**
	 * Display the breadcrumbs trail.
	 *
	 * Depends on Breadcrumb NavXT plugin being installed.
	 */
	function goodtailor_breadcrumbs() {
		// Check if the Breadcrumb NavXT plugin is installed.
		if ( ! function_exists( 'bcn_display' ) ) {
			return;
		}
		// Check if really should show the breadcrumbs.
		if ( ! is_customize_preview() && 'hide' === goodtailor_get_setting( 'show_breadcrumbs' ) ) {
			return;
		}
		?>
		<div class="breadcrumbs" typeof="BreadcrumbList" vocab="http://schema.org/">
			<?php bcn_display(); ?>
		</div>
		<?php
	}
endif;

if ( ! function_exists( 'goodtailor_cart_link' ) ) :
	/**
	 * Displays the cart link.
	 *
	 * Depends on WooCommerce plugin being installed.
	 */
	function goodtailor_cart_link() {
		?>
		<a class="cart-contents" href="<?php echo esc_url( wc_get_cart_url() ); ?>" title="<?php esc_attr_e( 'View your shopping cart', 'goodtailor' ); ?>">
			<?php
			$count = WC()->cart->get_cart_contents_count();
			if ( $count ) :
				?>
				<span class="count"><?php echo esc_html( $count ); ?></span>
			<?php endif; ?>
		</a>
		<?php
	}
endif;

if ( ! function_exists( 'goodtailor_pagination' ) ) :
	/**
	 * Displays the paging navigation on archive pages.
	 */
	function goodtailor_pagination() {
		the_posts_pagination( array(
			'prev_text'          => '<i class="typcn typcn-arrow-left"></i><span class="screen-reader-text">' . esc_html__( 'Previous page', 'goodtailor' ) . '</span>',
			'next_text'          => '<i class="typcn typcn-arrow-right"></i><span class="screen-reader-text">' . esc_html__( 'Next page', 'goodtailor' ) . '</span>',
			'before_page_number' => '<span class="meta-nav screen-reader-text">' . esc_html__( 'Page', 'goodtailor' ) . ' </span>',
		) );
	}
endif;

if ( ! function_exists( 'goodtailor_post_meta' ) ) :
	/**
	 * Displays meta information related to the post.
	 */
	function goodtailor_post_meta() {
		?>

		<footer class="entry-meta">

			<?php if ( is_sticky() && is_home() ) : ?>
				<span class="entry-sticky-badge"><?php echo esc_html__( 'Featured post', 'goodtailor' ); ?></span>
			<?php endif; ?>

			<time class="entry-date" datetime="<?php echo esc_attr( get_the_time( 'c' ) ); ?>">
				<a href="<?php the_permalink(); ?>" rel="bookmark"><?php echo esc_html( get_the_date() ); ?></a>
			</time>

			<span class="entry-meta-separator">/</span>

			<span class="entry-author">
				<?php
				/* translators: %s is the post author */
				printf( esc_html__( 'Author: %s', 'goodtailor' ), wp_kses_post( get_the_author_posts_link() ) );
				?>
			</span>

			<?php
			/* translators: used between list items, there is a space after the comma */
			$categories = get_the_category_list( __( ', ', 'goodtailor' ) );
			if ( $categories ) :
				?>

				<span class="entry-meta-separator">/</span>

				<span class="entry-categories">
					<?php
					/* translators: %s is a list of categories */
					printf( esc_html__( 'Category: %s', 'goodtailor' ), wp_kses_post( $categories ) );
					?>
				</span>

			<?php endif; ?>

			<?php
			$comments_number = get_comments_number();
			if ( ! is_single() && $comments_number ) :
				?>

				<span class="entry-meta-separator">/</span>

				<a href="<?php comments_link(); ?>">
					<span class="entry-comments">
						<?php
						/* translators: %d is the number of comments */
						printf( esc_html( _n( '%d comment', '%d comments', $comments_number, 'goodtailor' ) ), esc_html( $comments_number ) );
						?>
					</span>
				</a>

			<?php endif; ?>

		</footer>

		<?php
	}
endif;

if ( ! function_exists( 'goodtailor_render_comment' ) ) :
	/**
	 * Displays a comment.
	 */
	function goodtailor_render_comment( $comment, $args, $depth ) {
		?>

		<li id="comment-<?php comment_ID(); ?>" <?php comment_class( $comment->has_children ? 'parent' : '', $comment ); ?>>
			<article id="div-comment-<?php comment_ID(); ?>" class="comment-body">

				<footer class="comment-footer">

					<div class="comment-author vcard">
						<?php
						if ( 0 !== $args['avatar_size'] ) {
							echo get_avatar( $comment, $args['avatar_size'], '', '', array( 'class' => 'comment-author-avatar' ) );
						}
						echo '<span class="comment-author-name fn">' . get_comment_author_link( $comment ) . '</span>';
						if ( goodtailor_is_post_author( $comment->user_id ) ) {
							echo '<span class="comment-author-label">' . esc_html_x( 'Author', 'comment author label', 'goodtailor' ) . '</span>';
						}
						?>
					</div>

					<div class="comment-date">
						<time datetime="<?php comment_time( 'c' ); ?>">
							<a href="<?php echo esc_url( get_comment_link( $comment, $args ) ); ?>">
								<?php echo esc_html( goodtailor_to_relative_date( $comment->comment_date_gmt ) ); ?>
							</a>
						</time>
					</div>

					<div class="comment-actions">
						<?php
						edit_comment_link( esc_html__( 'Edit', 'goodtailor' ) );

						comment_reply_link( array_merge( $args, array(
							'add_below' => 'div-comment',
							'depth'     => $depth,
						) ) );
						?>
					</div>

					<?php if ( '0' === $comment->comment_approved ) : ?>
						<p class="comment-awaiting-moderation"><?php esc_html_e( 'Your comment is awaiting moderation.', 'goodtailor' ); ?></p>
					<?php endif; ?>

				</footer>

				<div class="comment-content">
					<?php comment_text(); ?>
				</div>

			</article>

		<?php
	}
endif;

if ( ! function_exists( 'goodtailor_header_info_block' ) ) :
	/**
	 * Displays a header info block with text and icon that were set via the Customizer.
	 */
	function goodtailor_header_info_block( $id ) {
		$text = get_theme_mod( 'header_info_block_text_' . $id );
		if ( $text ) {
			$icon_class = get_theme_mod( 'header_info_block_icon_class_' . $id );
			$icon_style = get_theme_mod( 'header_info_block_icon_style_' . $id );
			?>

			<div class="header-info-block js-header-info-block-<?php echo esc_attr( $id ); ?>">

				<?php if ( $icon_class || is_customize_preview() ) : ?>
					<div class="header-info-block-icon <?php if ( $icon_style ) { echo 'header-info-block-icon--' . esc_attr( $icon_style ); } ?>">
						<span class="<?php echo esc_attr( $icon_class ); ?>"></span>
					</div>
				<?php endif; ?>

				<div class="header-info-block-text">
					<?php echo wp_kses_post( goodtailor_html_entity_decode( $text ) ); ?>
				</div>

			</div>

			<?php
		} elseif ( is_customize_preview() ) {
			echo '<div class="header-info-block-placeholder js-header-info-block-' . esc_attr( $id ) . '"></div>';
		}
	}
endif;

if ( ! function_exists( 'goodtailor_shop_sidebar' ) ) :
	/**
	 * Displays Shop sidebar.
	 */
	function goodtailor_shop_sidebar() {
		if ( ! is_active_sidebar( 'shop-sidebar-1' ) ) {
			return;
		}
		?>

		<div class="sidebar-wrap">
			<aside id="secondary" class="sidebar-content">
				<?php dynamic_sidebar( 'shop-sidebar-1' ); ?>
			</aside>
		</div>

		<?php
	}
endif;
