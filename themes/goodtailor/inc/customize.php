<?php
/**
 * Customizer settings.
 *
 * @package GoodTailor
 */

/**
 * Register customizer panels, sections and controls
 */
function goodtailor_customize_register( $wp_customize ) {
	global $goodtailor_defaults;

	// Register custom controls.
	require_once get_template_directory() . '/inc/customizer/class-rgba-color-control.php';
	$wp_customize->register_control_type( 'GoodTailor_Customize_RGBA_Color_Control' );

	$wp_customize->remove_control( 'header_textcolor' );
	$wp_customize->get_setting( 'blogname' )->transport = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport = 'postMessage';
	$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';
	$wp_customize->selective_refresh->add_partial( 'site_branding_text', array(
		'settings'            => array( 'blogname', 'blogdescription' ),
		'selector'            => '.site-branding-text',
		'container_inclusive' => true,
		'render_callback'     => 'goodtailor_site_branding_text',
	) );

	// Light custom logo for dark backgrounds
	$wp_customize->add_setting( 'custom_logo_light', array(
		'type'              => 'theme_mod',
		'transport'         => 'postMessage',
		'sanitize_callback' => 'absint',
	) );
	$custom_logo_control = $wp_customize->get_control( 'custom_logo' );
	$wp_customize->add_control( new WP_Customize_Cropped_Image_Control( $wp_customize, 'custom_logo_light', array(
		'label'         => esc_html__( 'Light logo (for dark backgrounds)', 'goodtailor' ),
		'section'       => $custom_logo_control->section,
		'priority'      => $custom_logo_control->priority + 1,
		'height'        => $custom_logo_control->height,
		'width'         => $custom_logo_control->width,
		'flex_height'   => $custom_logo_control->flex_height,
		'flex_width'    => $custom_logo_control->flex_width,
		'button_labels' => $custom_logo_control->button_labels,
	) ) );
	array_push( $wp_customize->selective_refresh->get_partial( 'custom_logo' )->settings, 'custom_logo_light' );

	// Custom colors
	$wp_customize->add_setting( 'primary_color', array(
		'type'              => 'theme_mod',
		'transport'         => 'postMessage',
		'default'           => $goodtailor_defaults['primary_color'],
		'sanitize_callback' => function( $color ) {
			return sanitize_hex_color( $color ? $color : goodtailor_get_default( 'primary_color' ) );
		},
	) );
	$wp_customize->add_setting( 'primary_color_light', array(
		'type'              => 'theme_mod',
		'transport'         => 'postMessage',
		'default'           => $goodtailor_defaults['primary_color_light'],
		'sanitize_callback' => function( $color ) {
			return sanitize_hex_color( $color ? $color : goodtailor_get_default( 'primary_color_light' ) );
		},
	) );
	$wp_customize->add_setting( 'primary_button_color_1', array(
		'type'              => 'theme_mod',
		'transport'         => 'postMessage',
		'default'           => $goodtailor_defaults['primary_button_color_1'],
		'sanitize_callback' => function( $color ) {
			return sanitize_hex_color( $color ? $color : goodtailor_get_default( 'primary_button_color_1' ) );
		},
	) );
	$wp_customize->add_setting( 'primary_button_color_2', array(
		'type'              => 'theme_mod',
		'transport'         => 'postMessage',
		'default'           => $goodtailor_defaults['primary_button_color_2'],
		'sanitize_callback' => function( $color ) {
			return sanitize_hex_color( $color ? $color : goodtailor_get_default( 'primary_button_color_2' ) );
		},
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'primary_color', array(
		'label'   => esc_html__( 'Primary color', 'goodtailor' ),
		'section' => 'colors',
	) ) );

	// Header
	$wp_customize->get_section( 'header_image' )->title = esc_html__( 'Header', 'goodtailor' );
	$wp_customize->add_setting( 'header_bg_color', array(
		'type'              => 'theme_mod',
		'transport'         => 'postMessage',
		'sanitize_callback' => 'goodtailor_sanitize_rgba_color',
		'default'           => $goodtailor_defaults['header_bg_color'],
	) );
	$wp_customize->add_control( new GoodTailor_Customize_RGBA_Color_Control( $wp_customize, 'header_bg_color', array(
		'section'     => 'header_image',
		'label'       => esc_html__( 'Header background color', 'goodtailor' ),
		'description' => esc_html__( 'Overlaid on top of Header image. If you don\'t see header image, you may need to decrease the alpha value.', 'goodtailor' ),
	) ) );

	// Breadcrumbs
	$wp_customize->add_setting( 'show_breadcrumbs', array(
		'type'              => 'theme_mod',
		'transport'         => 'postMessage',
		'default'           => $goodtailor_defaults['show_breadcrumbs'],
		'sanitize_callback' => function( $value ) {
			return ( 'show' !== $value && 'hide' !== $value ) ? goodtailor_get_default( 'show_breadcrumbs' ) : $value;
		},
	) );
	$wp_customize->add_control( 'show_breadcrumbs', array(
		'type'    => 'select',
		'section' => 'header_image',
		'label'   => esc_html__( 'Show breadcrumbs?', 'goodtailor' ),
		'choices' => array(
			'show' => esc_html__( 'Show', 'goodtailor' ),
			'hide' => esc_html__( 'Hide', 'goodtailor' ),
		),
	) );

	// Header Layout
	$wp_customize->add_section( 'header_layout', array(
		'title'    => esc_html__( 'Header Layout', 'goodtailor' ),
		'priority' => 65, // After Header
	) );
	$wp_customize->add_setting( 'header_layout', array(
		'type'              => 'theme_mod',
		'default'           => $goodtailor_defaults['header_layout'],
		'sanitize_callback' => function( $value ) {
			return ( 'layout_standard' !== $value && 'layout_transparent' !== $value ) ? 'layout_standard' : $value;
		},
	) );
	$wp_customize->add_control( 'header_layout', array(
		'type'    => 'select',
		'section' => 'header_layout',
		'label'   => esc_html__( 'Global Header Layout', 'goodtailor' ),
		'choices' => array(
			'layout_standard'    => esc_html__( 'Standard', 'goodtailor' ),
			'layout_transparent' => esc_html__( 'Transparent', 'goodtailor' ),
		),
	) );
	$wp_customize->add_setting( 'header_color_style', array(
		'type'              => 'theme_mod',
		'default'           => $goodtailor_defaults['header_color_style'],
		'sanitize_callback' => function( $value ) {
			return ( 'bg_light' !== $value && 'bg_dark' !== $value ) ? 'bg_light' : $value;
		},
	) );
	$wp_customize->add_control( 'header_color_style', array(
		'type'    => 'select',
		'section' => 'header_layout',
		'label'   => esc_html__( 'Header Color Style', 'goodtailor' ),
		'choices' => array(
			'bg_light' => esc_html__( 'Dark text on Light background', 'goodtailor' ),
			'bg_dark'  => esc_html__( 'Light text on Dark background', 'goodtailor' ),
		),
	) );

	// Footer
	$wp_customize->add_section( 'footer', array(
		'title'    => esc_html__( 'Footer', 'goodtailor' ),
		'priority' => 70, // After Header Layout
	) );
	$wp_customize->add_setting( 'footer_bg_image_id', array(
		'type'              => 'theme_mod',
		'sanitize_callback' => 'absint',
	) );
	$wp_customize->add_control( new WP_Customize_Cropped_Image_Control( $wp_customize, 'footer_bg_image_id', array(
		'label'       => esc_html__( 'Footer background image', 'goodtailor' ),
		'section'     => 'footer',
		'width'       => 2000,
		'height'      => 900,
		'flex_width'  => true,
		'flex_height' => true,
	) ) );

	$wp_customize->add_setting( 'footer_bg_color', array(
		'type'              => 'theme_mod',
		'transport'         => 'postMessage',
		'sanitize_callback' => 'goodtailor_sanitize_rgba_color',
		'default'           => $goodtailor_defaults['footer_bg_color'],
	) );
	$wp_customize->add_control( new GoodTailor_Customize_RGBA_Color_Control( $wp_customize, 'footer_bg_color', array(
		'section'     => 'footer',
		'label'       => esc_html__( 'Footer background color', 'goodtailor' ),
		'description' => esc_html__( 'Overlaid on top of footer background image. If you don\'t see footer background image, you may need to decrease the alpha value.', 'goodtailor' ),
	) ) );

	$wp_customize->add_setting( 'footer_bottom_bg_color', array(
		'type'              => 'theme_mod',
		'transport'         => 'postMessage',
		'sanitize_callback' => 'goodtailor_sanitize_rgba_color',
		'default'           => $goodtailor_defaults['footer_bottom_bg_color'],
	) );
	$wp_customize->add_control( new GoodTailor_Customize_RGBA_Color_Control( $wp_customize, 'footer_bottom_bg_color', array(
		'section' => 'footer',
		'label'   => esc_html__( 'Footer bottom background color', 'goodtailor' ),
	) ) );

	// Footer tagline
	$wp_customize->add_setting( 'footer_tagline', array(
		'type'              => 'theme_mod',
		'transport'         => 'postMessage',
		'sanitize_callback' => 'goodtailor_sanitize_raw_html',
		'default'           => $goodtailor_defaults['footer_tagline'],
	) );
	$wp_customize->add_control( 'footer_tagline', array(
		'type'    => 'textarea',
		'section' => 'footer',
		'label'   => esc_html__( 'Footer tagline', 'goodtailor' ),
	) );
	$wp_customize->selective_refresh->add_partial( 'footer_tagline', array(
		'selector'        => '.site-footer .footer-tagline',
		'render_callback' => function() {
			echo wp_kses_post( goodtailor_get_footer_tagline() );
		},
	) );

	// Header Info Blocks
	$wp_customize->add_panel( 'header_info_blocks', array(
		'title'       => esc_html__( 'Header Info Blocks', 'goodtailor' ),
		'description' => esc_html__( 'Edit the quick information blocks shown in the header of the site (e.g. contact information)', 'goodtailor' ),
		'priority'    => 130, // Before Additional CSS
	) );
	goodtailor_add_header_info_block_section( $wp_customize, 1 );
	goodtailor_add_header_info_block_section( $wp_customize, 2 );
	goodtailor_add_header_info_block_section( $wp_customize, 3 );
}
add_action( 'customize_register', 'goodtailor_customize_register' );

/**
 * Create "Header Info Block" customizer section with the given ID
 */
function goodtailor_add_header_info_block_section( $wp_customize, $id ) {
	$wp_customize->add_section( 'header_info_block_' . $id, array(
		/* translators: %d is an index */
		'title' => sprintf( esc_html__( 'Info Block %d', 'goodtailor' ), $id ),
		'panel' => 'header_info_blocks',
	) );

	$wp_customize->add_setting( 'header_info_block_text_' . $id, array(
		'type'              => 'theme_mod',
		'transport'         => 'postMessage',
		'sanitize_callback' => 'goodtailor_sanitize_raw_html',
	) );
	$wp_customize->add_control( 'header_info_block_text_' . $id, array(
		'type'    => 'textarea',
		'section' => 'header_info_block_' . $id,
		'label'   => esc_html__( 'Text', 'goodtailor' ),
	) );
	$wp_customize->selective_refresh->add_partial( 'header_info_block_text_' . $id, array(
		'selector'            => '.js-header-info-block-' . $id,
		'container_inclusive' => true,
		'render_callback'     => function( $partial ) {
			goodtailor_header_info_block( str_replace( 'header_info_block_text_', '', $partial->id ) );
		},
	) );

	$wp_customize->add_setting( 'header_info_block_icon_class_' . $id, array(
		'type'              => 'theme_mod',
		'transport'         => 'postMessage',
		'sanitize_callback' => 'goodtailor_sanitize_html_classes',
	) );
	$allowed_html = array( 'a' => array( 'href' => true, 'target' => true ), 'code' => array(), 'br' => array() );
	/* translators: 1: is a link to the icon font website, 2: is the name of the icon font, 3: is an example code snippet */
	$format = __( '<a href="%1$s" target="_blank">%2$s</a>, e.g. <code>%3$s</code>', 'goodtailor' );
	$available_fonts = implode( '<br>', array(
		sprintf( $format, 'https://fontawesome.com/v4.7.0/icons/', 'FontAwesome', 'fa&nbsp;fa-envelope' ),
		sprintf( $format, 'http://typicons.com/', 'Typicons', 'typcn&nbsp;typcn-mail' ),
		sprintf( $format, 'https://themify.me/themify-icons', 'Themify Icons', 'ti-email' ),
	) );
	$wp_customize->add_control( 'header_info_block_icon_class_' . $id, array(
		'type'        => 'text',
		'section'     => 'header_info_block_' . $id,
		'label'       => esc_html__( 'Icon Font CSS class', 'goodtailor' ),
		/* translators: %s is a list of available icon fonts with usage examples */
		'description' => sprintf( wp_kses( __( 'Enter a valid icon font CSS class.<br> Available icon fonts:<br> %1$s', 'goodtailor' ), $allowed_html ), $available_fonts ),
	) );

	$wp_customize->add_setting( 'header_info_block_icon_style_' . $id, array(
		'type'              => 'theme_mod',
		'default'           => 'normal',
		'transport'         => 'postMessage',
		'sanitize_callback' => function( $value ) {
			return ( 'normal' !== $value && 'circled' !== $value ) ? 'normal' : $value;
		},
	) );
	$wp_customize->add_control( 'header_info_block_icon_style_' . $id, array(
		'type'    => 'select',
		'section' => 'header_info_block_' . $id,
		'label'   => esc_html__( 'Icon style', 'goodtailor' ),
		'choices' => array(
			'normal'  => esc_html__( 'Normal', 'goodtailor' ),
			'circled' => esc_html__( 'In circle', 'goodtailor' ),
		),
	) );
}

/**
 * Sanitize raw HTML code
 */
function goodtailor_sanitize_raw_html( $value ) {
	return goodtailor_htmlspecialchars( stripslashes( $value ) );
}

/**
 * Sanitize a list of HTML classes
 */
function goodtailor_sanitize_html_classes( $classes ) {
	if ( is_string( $classes ) ) {
		$classes = explode( ' ', $classes );
	}
	if ( is_array( $classes ) && count( $classes ) > 0 ) {
		return implode( ' ', array_map( 'sanitize_html_class', $classes ) );
	}
	return sanitize_html_class( $classes );
}

/**
 * Sanitize color in CSS rgba(...) format
 */
function goodtailor_sanitize_rgba_color( $rgba ) {
	// Remove whitespace
	$rgba = preg_replace( '/\s+/', '', $rgba );
	if ( preg_match( '/rgba\(\d+,\d+,\d+,(?:[01](?:\.\d+)?|[01]?(?:\.\d+))\)/', $rgba ) ) {
		return $rgba;
	} else {
		return '';
	}
}

/**
 * Load dynamic logic for the customizer controls area
 */
function goodtailor_customize_controls_scripts() {
	wp_enqueue_style( 'goodtailor-customize-controls', get_template_directory_uri() . '/assets/customizer/customize-controls.css', array( 'customize-controls' ), GOODTAILOR_THEME_VERSION );
	wp_enqueue_script( 'goodtailor-customize-controls', get_template_directory_uri() . '/assets/customizer/customize-controls.js', array( 'customize-controls' ), GOODTAILOR_THEME_VERSION, true );
}
add_action( 'customize_controls_enqueue_scripts', 'goodtailor_customize_controls_scripts' );

/**
 * Bind JS handlers to instantly live-preview changes
 */
function goodtailor_customize_preview_scripts() {
	wp_enqueue_script( 'goodtailor-customize-preview', get_template_directory_uri() . '/assets/customizer/customize-preview.js', array( 'customize-preview' ), GOODTAILOR_THEME_VERSION, true );
}
add_action( 'customize_preview_init', 'goodtailor_customize_preview_scripts' );
