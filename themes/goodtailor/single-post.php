<?php
/**
 * The template for displaying single posts.
 *
 * @package GoodTailor
 */

get_header();
?>

<div class="container">

	<div class="main-wrap">
		<main id="main" class="main-content">

			<?php
			while ( have_posts() ) : the_post();

				get_template_part( 'partials/content/content-post', get_post_format() );

				if ( comments_open() || get_comments_number() ) :
					comments_template();
				endif;

			endwhile;
			?>

		</main>
	</div>

	<?php get_sidebar(); ?>

</div>

<?php
get_footer();
